'use strict';

let express = require('express');
let router = express.Router();

let products = require('../models/products');
let categories = require('../models/products-category');

router.get('/api/products', (req, res) => {
  products.find((err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send(result);
    }
  });
});

router.get('/api/products/:id', (req, res) => {
  products.findOne({ _id: req.params.id },(err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send(result);
    }
  });
});


router.post('/api/products', (req, res) => {
  categories.findOne({_id: req.body.categoryId}, (err, category) => {
    let newProducts = new products({
      businessId: req.body.businessId,
      name: req.body.name,
      portions: req.body.portions,
      price: req.body.price,
      categoryId: req.body.categoryId,
      printer: category.printer,
      portions: req.body.portions
    });

    newProducts.save( (err) => {
      if (err) return err;
    })
    .then(() => {
      res.writeHead(200, { 'Content-Type': 'text/plain' });
      res.end('Se ha guardado con éxito');
    })
    .catch(() => {
      res.write('Ha ocurrido un error.');
    });
  });
});

router.put('/api/products/:id', (req, res) => {
  categories.findOne({_id: req.body.categoryId}, (err, category) => {
    req.body.printer = category.printer;
    products.update({'_id': req.params.id}, req.body, (err, result) => {
      if (err) {
        return err;
      } else {
        products.update({ '_id': req.params.id}, {$set: {printer: category.printer}}, (err, result) => {
          if (err) {
            res.write(err);
          } else {
            res.writeHead(200, { 'Content-Type': 'text/plain' });
            res.end('Se ha actualizado con éxito.');
          }
        });
      }
    });
  });
});

router.delete('/api/products/:id', (req, res) => {
  products.remove({'_id': req.params.id}, (err) => {
    if (err) {
      return err;
    } else {
      res.writeHead(200, { 'Content-Type': 'text/plain' });
      res.end('Se ha eliminado con éxito.');
    }
  });
});

module.exports = router;
