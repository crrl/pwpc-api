'use strict';

let express = require('express');
let router = express.Router();

let rosterHistory = require('../models/roster-history');
let roster = require('../models/roster-details');


router.get('/api/roster/globalUser/:id', (req, res) => {
  roster.findOne({'userId':req.params.id}, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      rosterHistory.find({rosterId: result._id}, (err, result) => {
        if (err) {
          res.write(err);
        } else {
          res.send(result);
        }
      })
    }
  });
});


router.post('/api/rosterHistory', (req, res) => {
  roster.find({'_id': req.body.rosterId}, (err, result) => {
    if (err) {
      res.write('Ha ocurrido un error en la transacción.');
    } else {
      req.body.total = req.body.total.replace(',','');
      let rosterToSave = new rosterHistory({
        businessId: req.body.businessId,
        rosterId: req.body.rosterId,
        userId: req.body.userId,
        username: req.body.username,
        workedDays: req.body.workedDays,
        extraTime: req.body.extraTime,
        total: req.body.total,
        absenceDays: req.body.absenceDays,
        initialDate: result[0].initialDate
      });
      rosterToSave.save((err) => {
        if (err) {
          return err;
        }
      })
      .then((response) => {
        roster.update({'_id': req.body.rosterId}, {$set: {
          initialDate:Date.now(),
          total: 0,
          workedDays: 0,
          extraTime: 0,
          absenceDays: 0,
        }}, (err,result) => {
          if (err) {
            return err;
          } else {
            res.send('Se ha guardado con éxito.');
          }
        });
      });
    }
  });
});


module.exports = router;
