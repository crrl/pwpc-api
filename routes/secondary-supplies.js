'use strict';

let express = require('express');
let router = express.Router();

let supplies = require('../models/secondary-supplies');

router.get('/api/secundary-supplies', (req, res) => {
  supplies.find((err, result) => {
    if (err) {
      return err;
    } else {
      res.send(result);
    }
  });
});


router.post('/api/secundary-supplies', (req, res) => {
  let supplyId;
  let newSupplies = new supplies({
    businessId: req.body.businessId,
    supplyId: req.body.supplyId,
    name: req.body.name,
    supplyName: req.body.supplyName,
    portions: req.body.portions
  });

  newSupplies.save( (err, result) => {
    if (err) {
      return err;
    } else {
      supplyId = result._id;
    }
  })
    .then(() => {
      res.writeHead(200, {'Content-Type': 'text/plain'});
      res.end('Se ha agregado con éxito.');
    })
    .catch(() => {
      return err;
    });
  });

router.put('/api/secundary-supplies/:id', (req, res) => {
  
  supplies.update({'_id': req.params.id}, req.body, (err, result) => {
    if (err) {
      return err;
    } else {
      res.writeHead(200, { 'Content-Type': 'text/plain' });
      res.end('Se ha actualizado con éxito.');
    }
  });
});

router.delete('/api/secundary-supplies/:id', (req, res) => {
  supplies.remove({'_id': req.params.id}, (err) => {
    if (err) {
      return err;
    }
  })
  .then(() => {
      res.writeHead(200, { 'Content-Type': 'text/plain' });
      res.end('Se eliminó correctamente.');
    })
  .catch(() => {
    res.write('Ha ocurrido un error.');
  });
});

module.exports = router;
