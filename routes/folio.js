'use strict';

let express = require('express');
let router = express.Router();

let folio = require('../models/folio');
let folioPending = require('../models/folio-pending');
let servingPendings = require('../models/serving-pending');
let internFolio = require('../models/internal-folio');
let supplies = require('../models/supplies');
let _ = require('lodash');
let socketApi = require('../socket');
let io = socketApi.io;
let ticket  = false;

router.get('/api/folioHistory', (req, res) => {
  let serviceResponse;
  if (req.query.status) {
    folio.find({status: req.query.status},(err, result) => {
      if (err) {
        res.write(err);
      } else {
        folio.count({status: req.query.status},(err,count) => {
          if (err) {
            res.write(err);
          } else {
            result = result.reverse();
            res.send({result,count});          
          }
        });
      }
    })
    .sort([['date', -1]])
    .skip(parseInt(req.query.skip))
    .limit(parseInt(6));  
  } else {
    folio.find((err, result) => {
      if (err) {
        res.write(err);
      } else {
        folio.count((err,count) => {
          if (err) {
            res.write(err);
          } else {
            result = result.reverse();
            res.send({result,count});          
          }
        });
      }
    })
    .sort([['date', -1]])
    .skip(parseInt(req.query.skip))
    .limit(parseInt(6));
  }
});


router.get('/api/folioTicket/:id', (req, res) => {
  folioPending.findOne({_id: req.params.id}, (err, folioResult) => {
    if (err) {
      res.write(err);
    } else {
      folioResult.details.forEach( (detail) => {
        detail.products.forEach( (product) => {
          product.printed = true;
        });
      });
      folioPending.update({ _id: req.params.id }, folioResult, (err, result) => {
        if (err) {
          res.write(err);
        } else {
          if (folioResult.table === 9998) return;
          folio.findOne({ _id: folioResult.folioId }, (err, result) => {
            if (err) {
              res.write(err);
            } else {
              result.details.forEach( (detail) => {
                detail.products.forEach( (product) => {
                  product.printed = true;
                });
              });
              folio.update({ _id: folio.folioId }, result, (err, result) => {
                if (err) {
                  res.write(err);
                } else {
                  folioPending.find((err, result) => {
                    if (err) {
                      res.write(err);
                    } else {
                      io.sockets.emit('refreshPendingPayment', {msg: result});
                      res.send('Se ha guardado con éxito');
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
});


router.get('/api/folioTicketBack/:id', (req, res) => {
  folioPending.findOne({_id: req.params.id}, (err, folioResult) => {
    if (err) {
      res.write(err);
    } else {
      folioResult.details.forEach( (detail) => {
        detail.products.forEach( (product) => {
          product.printed = true;
        });
      });
      folioPending.update({ _id: req.params.id }, folioResult, (err, result) => {
        if (err) {
          res.write(err);
        } else {
          if (folioResult.table === 9998) return;
          folio.findOne({ _id: folioResult.folioId }, (err, result) => {
            if (err) {
              res.write(err);
            } else {
              result.details.forEach( (detail) => {
                detail.products.forEach( (product) => {
                  product.printed = true;
                });
              });
              folio.update({ _id: folio.folioId }, result, (err, result) => {
                if (err) {
                  res.write(err);
                } else {
                  folioPending.find((err, result) => {
                    if (err) {
                      res.write(err);
                    } else {
                      res.send('Se ha guardado con éxito');
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
});

router.get('/api/folioInternHistory', (req, res) => {
  let serviceResponse;
  if (req.query.status) {
    internFolio.find({status: req.query.status},(err, result) => {
      if (err) {
        res.write(err);
      } else {
        internFolio.count({status: req.query.status},(err,count) => {
          if (err) {
            res.write(err);
          } else {
            result = result.reverse();
            res.send({result,count});          
          }
        });
      }
    })
    .sort([['date', -1]])
    .skip(parseInt(req.query.skip))
    .limit(parseInt(6));
  } else {
    internFolio.find((err, result) => {
      if (err) {
        res.write(err);
      } else {
        internFolio.count((err,count) => {
          if (err) {
            res.write(err);
          } else {
            result = result.reverse();
            res.send({result,count});          
          }
        });
      }
    })
    .sort([['date', -1]])
    .skip(parseInt(req.query.skip))
    .limit(parseInt(6));
  }
});



router.get('/api/folioHistory/:id', (req, res) => {
  folio.findOne({_id: req.params.id}, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send(result);
    }
  });
});

router.get('/api/folioHistory/:initialDate/:finalDate', (req, res) => {
  let initialDate = new Date(req.params.initialDate + ' 00:00:00').toISOString();
  let finalDate = new Date(req.params.finalDate + ' 23:59:59').toISOString();
  folio.find({'date': {'$gte' : initialDate, '$lt': finalDate }}, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      folio.count({'date': {'$gte' : initialDate, '$lt': finalDate }},(err,count) => {
        if (err) {
          res.write(err);
        } else {
          result = result.reverse();
          res.send({result,count});          
        }
      });
    }
  })
  .sort([['date', -1]]);  
});


router.get('/api/folioInternHistory/:initialDate/:finalDate', (req, res) => {
  let initialDate = new Date(req.params.initialDate + ' 00:00:00').toISOString();
  let finalDate = new Date(req.params.finalDate + ' 23:59:59').toISOString();
  internFolio.find({'date': {'$gte' : initialDate, '$lt': finalDate }}, (err, result) => {
    if (err) {
      res.write(err);
    }else {
      internFolio.count({'date': {'$gte' : initialDate, '$lt': finalDate }},(err,count) => {
        if (err) {
          res.write(err);
        } else {
          result = result.reverse();
          res.send({result,count});          
        }
      });
    }
  })
  .sort([['date', -1]]);  
});

router.get('/api/folio', (req, res) => {
  let serviceResponse;
  let date;
  let dateString = new Date();
  dateString = dateString.toISOString();
  dateString = JSON.stringify(dateString);

  dateString = dateString.substring(1,11) + 'T23:59:59.999Z';
  date = new Date(dateString);
  folio.find({'date': {'$gt' : date}}, (err, result) => {
    if (err) {
      res.write(err);
    }else {
      if (result.length > 0) {
        res.writeHead(500, {'Content-Type': 'text/plain'});
        res.end('Fechas inválidas.');
        return;
      }
      folioPending.find((err, result) => {
        if (err) {
          res.write(err);
        } else {
          res.send(result);
        }
      });
    }
  });
});

router.post('/api/folio', (req, res) => {
  let folioToSave;
  let folioPaid = false;
  
  folio.findOne({}, {}, { sort: { 'folio' : -1 } }, (err, item) => {
    let count = item && item.folio + 1;
    if (!count) {
      count = 1;
    }
    if (req.body.internalFood && !req.body.internalUser) {
      res.status(400).send({'validItems': false, 'status':400});
      return;
    }
    if (req.body.internalFood) {
      folioToSave = new internFolio({
        businessId: req.body.businessId,
        userId: req.body.userId,
        userName: req.body.userName,
        total: req.body.total,
        table: req.body.table,
        folio: count,
        details: req.body.details,
        dayName: req.body.dayName,
        status: 'Activa',
        internalFood: req.body.internalFood || false,
        internalUser: req.body.internalUser || ''
      });
    } else {
      req.body.details.forEach((detail) => {
        detail.products.forEach((product) => {
            product.printed = true;
        });
      });
      if (req.body.sendToKitchen) folioPaid = true;
      folioToSave = new folio({
        businessId: req.body.businessId,
        userId: req.body.userId,
        userName: req.body.userName,
        total: req.body.total,
        table: req.body.table,
        folio: count,
        paid: folioPaid,
        details: req.body.details,
        dayName: req.body.dayName,
        status: 'Activa'
      });
    }

    folioToSave.save((err, result) => {
      if (err) {
        res.write(err);
      } else {
        if (req.body.sendToKitchen) {
          res.send('Se ha guardado correctamente');
          return;
        }
        if (req.body.internalFood) {
          ticket = true;
        } else {
          ticket = false;
        }
        req.body.details.forEach((detail) => {
          detail.products.forEach((product) => {
              product.printed = false;
          });
        });
        let folioPendingToSave = new folioPending({
          folioId: result._id,
          businessId: req.body.businessId,
          userId: req.body.userId,
          userName: req.body.userName,
          total: req.body.total,
          table: req.body.table,
          folio: count,
          ticket: ticket,
          pendingPayment: req.body.pendingPayment,
          details: req.body.details
        });
        folioPendingToSave.save((err, result2) => {
          if (err) {
            res.write(err);
          } else {
            let servingPendingsToSave = new servingPendings({
              folioId: result._id,
              table: req.body.table,
              served: 'no',
              details: req.body.details
            });
            servingPendingsToSave.save((err, result) => {
              if (err) {
                res.write(err);
              } else {
                servingPendings.find((err, result) => {
                  if (err) {
                    res.write(err);
                  } else {
                    io.sockets.emit('refreshPendingServe', {msg: result});
                    folioPending.find((err, result) => {
                      if (err) {
                        res.write(err);
                      } else {
                        io.sockets.emit('refreshPendingPayment', {msg: result});

                        res.send('Se ha guardado con éxito');
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  });
});

router.put('/api/folioPending/ticket/:id', (req, res) => {
  folioPending.update({ '_id': req.params.id}, {$set: {ticket: req.body.ticket}}, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send(result);
    }
  });
});

router.put('/api/folio/:id', (req, res) => {
  let folioId;
  if (req.body.internalFood) {
    req.body.ticket = true;
  }
  folioPending.update({ '_id': req.params.id }, req.body, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      folioPending.findOne({ '_id': req.params.id}, (err, result) => {
        if (err) {
          res.write(err);
        } else {
          folioId = result.folioId;
          if (req.body.internalFood) {
            internFolio.update({ '_id': folioId}, req.body, (err, result2) => {
              if (err) {
                res.write(err)
              } else {
                req.body.served = 'no';
                servingPendings.update({ 'folioId': folioId }, req.body, (err, result3) => {
                  if (err) {
                    res.write(err);
                  } else {
                    servingPendings.find((err, result) => {
                      if (err) {
                        res.write(err);
                      } else {
                        io.sockets.emit('refreshPendingServe', {msg: result});
                        folioPending.find((err, result) => {
                          if (err) {
                            res.write(err);
                          } else {
                            io.sockets.emit('refreshPendingPayment', {msg: result});
                            res.send('Se ha actualizado con éxito');
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          } else {
            folio.update({ '_id': folioId}, req.body, (err, result2) => {
              if (err) {
                res.write(err)
              } else {
                req.body.served = 'no';
                servingPendings.update({ 'folioId': folioId }, req.body, (err, result3) => {
                  if (err) {
                    res.write(err);
                  } else {
                    servingPendings.find((err, result) => {
                      if (err) {
                        res.write(err);
                      } else {
                        io.sockets.emit('refreshPendingServe', {msg: result});
                        folioPending.find((err, result) => {
                          if (err) {
                            res.write(err);
                          } else {
                            io.sockets.emit('refreshPendingPayment', {msg: result});
                            res.send('Se ha actualizado con éxito');
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        }
      });
    }
  });
});

router.put('/api/pendingFolio/:id', (req, res) => {
  folioPending.findOne({ _id:req.params.id }, (err, currentFolio) => {
    folio.update({'_id': currentFolio.folioId}, {$set: {
      endDate: new Date()
    }}, (err, result) => {
      if (err) {
        res.write(err);
      }
    });
  })
  folioPending.update({_id : req.params.id }, {$set: {
    pendingPayment: req.body.pendingPayment,
    details: req.body.details
  }}, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      folioPending.find((err, result) => {
        if (err) {
          res.write(err);
        } else {
          io.sockets.emit('refreshPendingPayment', {msg: result});
          res.send('Se ha actualizado con éxito');
        }
      });
    }
  });
});

router.delete('/api/pendingFolio/:id', (req, res) => {
  let hasPortions = false;
  let portionsToSave = [];
  folioPending.findOne({ _id: req.params.id }, (err, currentFolio) => {    
    folio.update({ _id: currentFolio.folioId }, {$set: {
      paid: true
    }}, (err, result) => {
      if (err) {
        res.write(err);
      } else {
        if(currentFolio.table === 9998) {
          currentFolio.details.forEach((detail, detailIndex, detailArray) => {
            detail.products.forEach((product, index, array) => {
              if (product.portions.length > 0)  {
                hasPortions = true;
                let productQuantity = product.quantity;
                product.portions.forEach((portion) => {
                  let portionsToSaveIndex = -1;
                  portionsToSaveIndex = _.findIndex(portionsToSave, {id: portion.id});
                  if (portionsToSaveIndex < 0) {
                    portionsToSave.push({
                      id: portion.id,
                      usedPortions: portion.quantity * productQuantity
                    });
                  } else {
                    portionsToSave[portionsToSaveIndex].usedPortions += portion.quantity * productQuantity;
                  }
                });
              }
            });
          });
          if (hasPortions) {
            portionsToSave.forEach((savePortion,portionIndex, portionArray) => {
              let portions = 0;
              let quantityLeft = 0;
              
              supplies.findOne({_id: savePortion.id}, (err, result) => {
                if (err) {
                  res.write(err);
                } else {
                  portions = result.portions - savePortion.usedPortions;
                  quantityLeft = portions / result.portionPerProduct;
                  supplies.update({ _id: savePortion.id }, {$set: {
                    portions: portions,
                    quantity: quantityLeft
                  }}, (err, result) => {
                    if (err) {
                      return err;
                    } else {
                      if (portionIndex === portionArray.length - 1) {
                        if (err) {
                          res.write(err);
                        } else {
                          folioPending.remove({'_id': req.params.id }, (err, result) => {
                            if (err) {
                              res.write(err);
                            } else {
                              folioPending.find((err, result) => {
                                if (err) {
                                  res.write(err);
                                } else {
                                  io.sockets.emit('refreshPendingPayment', {msg: result});
                                  res.send('Se ha actualizado con éxito');
                                }
                              });
                            }
                          });  
                        }
                      }
                    }
                  });
                }
              });
            });
          } else {
            folioPending.remove({'_id': req.params.id }, (err, result) => {
              if (err) {
                res.write(err);
              } else {
                folioPending.find((err, result) => {
                  if (err) {
                    res.write(err);
                  } else {
                    io.sockets.emit('refreshPendingPayment', {msg: result});
                    res.send('Se ha actualizado con éxito');
                  }
                });
              }
            });
          }
        } else {
          folioPending.remove({'_id': req.params.id }, (err, result) => {
            if (err) {
              res.write(err);
            } else {
              folioPending.find((err, result) => {
                if (err) {
                  res.write(err);
                } else {
                  io.sockets.emit('refreshPendingPayment', {msg: result});
                  res.send('Se ha actualizado con éxito');
                }
              });
            }
          });
        }
      }
    });
  });
  
});


router.put('/api/cancelPendingFolio', (req, res) => {
  folioPending.remove({ folioId : req.body.id }, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      servingPendings.remove({ folioId : req.body.id}, (err, result) => {
        if (err) {
          res.write(err);
        } else {
          if (req.body.table === 9998) {
            internFolio.update({ _id: req.body.id}, {$set: {
              status: 'Cancelada',
              userCanceledId: req.body.cancelUserId,
              cancelUser: req.body.cancelUser
            }}, (err, result) => {
              if (err) {
                res.write(err);
              } else {
                servingPendings.find((err, result) => {
                  if (err) {
                    res.write(err);
                  } else {
                    io.sockets.emit('refreshPendingServe', {msg: result});
                    folioPending.find((err, result) => {
                      if (err) {
                        res.write(err);
                      } else {
                        io.sockets.emit('refreshPendingPayment', {msg: result});
                        res.send('Se ha actualizado con éxito');
                      }
                    });
                  }
                });
              }
            });
          } else {
            folio.update({ _id: req.body.id}, {$set: {
              status: 'Cancelada',
              userCanceledId: req.body.cancelUserId,
              cancelUser: req.body.cancelUser
            }}, (err, result) => {
              if (err) {
                res.write(err);
              } else {
                servingPendings.find((err, result) => {
                  if (err) {
                    res.write(err);
                  } else {
                    io.sockets.emit('refreshPendingServe', {msg: result});
                    folioPending.find((err, result) => {
                      if (err) {
                        res.write(err);
                      } else {
                        io.sockets.emit('refreshPendingPayment', {msg: result});
                        res.send('Se ha actualizado con éxito');
                      }
                    });
                  }
                });
              }
            });
          }
        }
      });
    }
  });
})

router.get('/api/servingPending', (req, res) => {
  servingPendings.find((err, result) => {
    if (err) {
      res.write(err)
    } else {
      res.send(result)
    }
  });
});

router.put('/api/servingPending/:id', (req, res) => {
  servingPendings.update({ folioId: req.params.id}, {$set: {
      served: req.body.served,
      details: req.body.details
    } }, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      folioPending.update({ folioId: req.params.id}, {$set: {
          details: req.body.details
        } }, (err, result) => {
        if (err) {
          res.write(err);
        } else {
          servingPendings.find((err, result) => {
            if (err) {
              res.write(err);
            } else {
              io.sockets.emit('refreshPendingServe', {msg: result});
              folioPending.find((err, result) => {
                if (err) {
                  res.write(err);
                } else {
                  io.sockets.emit('refreshPendingPayment', {msg: result});
                  res.send('Se ha actualizado con éxito');
                }
              });
            }
          });
        }
      });
    }
  });
});

router.put('/api/servingPendingDetail/:id', (req, res) => {
  servingPendings.update({ folioId: req.params.id}, {$set: {
      details: req.body.details
    } }, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      folioPending.update({ folioId: req.params.id}, {$set: {
          details: req.body.details
        } }, (err, result) => {
        if (err) {
          res.write(err);
        } else {
          servingPendings.find((err, result) => {
            if (err) {
              res.write(err);
            } else {
              io.sockets.emit('refreshPendingServe', {msg: result});
              folioPending.find((err, result) => {
                if (err) {
                  res.write(err);
                } else {
                  io.sockets.emit('refreshPendingPayment', {msg: result});
                  res.send('Se ha actualizado con éxito');
                }
              });
            }
          });
        }
      });
    }
  });
});

module.exports = router;
