'use strict';

let express = require('express');
let router = express.Router();
let paydesk = require('../models/paydesk');

router.get('/api/paydesk', (req, res) => {
  paydesk.find((err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send(result);
    }
  });
});

router.post('/api/paydesk', (req, res) => {
  let newPay = new paydesk({
    fundAvailable : req.body.fundAvailable,
    previousFund: req.body.previousFund,
    withdrawal: req.body.withdrawal,
    deposit: req.body.deposit,
    sells: req.body.sells,
    compras: req.body.compras,
    rosters: req.body.rosters,
    cashFund: req.body.cashFund
  });

  newPay.save((err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send('Se ha guardado con éxito');
    }
  });
});

module.exports = router;
