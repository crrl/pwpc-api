'use strict';

let express = require('express');
let router = express.Router();
let productsPrinters = require('../models/printers');
let folioPending = require('../models/folio-pending');
let productsCategory = require('../models/products-category');

router.post('/api/printer', (req, res) => {
  productsPrinters.findOne({name: req.body.printerName}, (err, result) => {
    if (result) {
      res.writeHead(500, {'Content-Type': 'text/plain'});
      res.end('Ya existe Impresora.');
    } else {
      let newPrinter = new productsPrinters({
        name: req.body.printerName,
      });


      newPrinter.save((err, result) => {
        if (err) {
          res.write(err);
        } else {
          res.send('Se ha guardado correctamente');
        }
      });
    }
  });
});

router.get('/api/printer', (req, res) => {
  productsPrinters.find((err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send(result);
    }
  });
});

router.get('/api/printer/:id', (req, res) => {
  productsPrinters.findOne({_id: req.params.id}, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send(result);
    }
  });
});

router.put('/api/printer/:id', (req, res) => {
  folioPending.find((err, pending) => {
    if (err) {
      res.write(err);
    } else {
      if (pending.length > 0) {
        res.writeHead(500, {'Content-Type': 'text/plain'});
        res.end('Favor de finalizar todos los pedidos.');
        return;
      } else {
        productsPrinters.findOne({ _id: req.params.id }, (err, printer) => {
          if (err) {
            res.write(err);
          } else {
            productsCategory.find({printer: printer.name }, (err, inUse) => {
              if (err) {
                res.write(err);
              } else { 
                if (inUse.length > 0) {
                  res.writeHead(500, {'Content-Type': 'text/plain'});
                  res.end('Favor de eliminar la categoría antes de actualizar.');
                  return;
                } else { 
                  productsPrinters.findOne({name: req.body.name}, (err, result) => {
                    if (result) {
                      res.writeHead(500, {'Content-Type': 'text/plain'});
                      res.end('Ya existe Impresora.');
                    } else {
                      productsPrinters.update({_id: req.params.id}, req.body, (err, result) => {
                        if (err) {
                          res.write(err);
                        } else {
                          res.send(result);
                        }
                      });
                    }
                  });
                }
              }
            })
          }
        })
      }
    }
  });

});

router.delete('/api/printer/:id', (req, res) => {
  folioPending.find((err, pending) => {
    if (err) {
      res.write(err);
    } else {
      if (pending.length > 0) {
        res.writeHead(500, {'Content-Type': 'text/plain'});
        res.end('Favor de finalizar todos los pedidos.');
        return;
      } else {
        productsPrinters.findOne({ _id: req.params.id }, (err, printer) => {
          if (err) {
            res.write(err);
          } else {
            productsCategory.find({printer: printer.name }, (err, inUse) => {
              if (err) {
                res.write(err);
              } else { 
                if (inUse.length > 0) {
                  res.writeHead(500, {'Content-Type': 'text/plain'});
                  res.end('Favor de eliminar la categoría.');
                  return;
                } else {
                  productsPrinters.remove({_id: req.params.id}, (err, result) => {
                    if (err) {
                      res.write(err);
                    } else {
                      res.send('Se ha eliminado con éxito');
                    }
                  });
                }
              }
            })
          }
        });
      }
    }
  });
});

module.exports = router;
