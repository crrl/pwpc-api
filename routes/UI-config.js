'use strict';

var express = require('express');
var router = express.Router();
let UIConfig = require('../models/UI-config');

router.post('/api/config', function(req, res) {
  UIConfig.find({userId: req.body.userId}, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      if (result.length > 0) {
        UIConfig.update({userId: req.body.userId}, {$set: {
          color: req.body.color,
          background: req.body.background,
          sellUI: req.body.sellUI,
          printOrders: req.body.printOrders,
          saveOrders: req.body.saveOrders
        }}, (err, result) => {
          if (err) {
            res.write(err);
          } else {
            res.send('Se ha actualizado correctamente.');
          }
        });
      } else {
        let insertConfig = new UIConfig({
          businessId: req.body.businessId,
          userId : req.body.userId,
          color: req.body.color,
          background: req.body.background,
          sellUI: req.body.sellUI,
          printOrders: req.body.printOrders,
          saveOrders: req.body.saveOrders
        });

        insertConfig.save( (err) => {
          if (err) {
            res.write(err);
          } else {
            res.send('Se ha guardado con éxito');
          }
        });
      }
    }
  });
});

router.get('/api/config/:id', (req, res) => {
  UIConfig.findOne({userId: req.params.id}, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send(result);
    }
  });
});

module.exports = router;
