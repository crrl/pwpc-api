'use strict';

let express = require('express');
let router = express.Router();
let productsCategory = require('../models/products-category');
let productsPrinters = require('../models/printers');
let products = require('../models/products');

router.post('/api/product-category', (req, res) => {
  let newCategory = new productsCategory({
    category: req.body.category,
    image: req.body.image,
    printer: req.body.printerName
  });


  newCategory.save((err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send('Se ha guardado correctamente');
    }
  });
});

router.get('/api/product-category', (req, res) => {
  productsCategory.find((err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send(result);
    }
  });
});

router.get('/api/product-category/:id', (req, res) => {
  productsCategory.findOne({_id: req.params.id}, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send(result);
    }
  });
});

router.put('/api/product-category/:id', (req, res) => {
  productsCategory.update({_id: req.params.id}, req.body, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send(result);
    }
  });
});

router.delete('/api/product-category/:id', (req, res) => {
  products.find({ categoryId: req.params.id },(err, product) => {
    if (err) {
      res.write(err);
    } else {
      if (product.length > 0) {
        res.writeHead(500, {'Content-Type': 'text/plain'});
        res.end('Está siendo utilizado por un producto.');
        return;
      } else {
        productsCategory.remove({_id: req.params.id}, (err, result) => {
          if (err) {
            res.write(err);
          } else {
            res.send('Se ha eliminado con éxito');
          }
        });
      }
    }
  });
});

module.exports = router;
