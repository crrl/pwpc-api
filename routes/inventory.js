'use strict';

let express = require('express');
let router = express.Router();

let inventory = require('../models/inventory');
let actualSupplies = require('../models/supplies');
let supplies = require('../models/supplies');
const moment = require('moment');
let _ = require('lodash');

router.get('/api/inventory', (req, res) => {
  inventory.find((err, result) => {
    if (err) {
      res.write(err);
    } else {
      inventory.count((err,count) => {
        if (err) {
          res.write(err);
        } else {
          result = result.reverse();
          res.send({result,count});          
        }
      });
    }
  })
  .sort([['date', -1]])
  .skip(parseInt(req.query.skip))
  .limit(parseInt(5))
});

router.get('/api/inventory/:id', (req, res) => {
  inventory.findOne({'_id': req.params.id}, (err, result) => {
    if (err) {
      res.write('Ha ocurrido un error.');
    } else {
      res.send(result);
    }
  });
});

router.post('/api/inventory', (req, res) => {
  if (!req.body.businessId || !req.body.userId ||
      !req.body.userName || !req.body.details) {
        res.writeHead(400, { 'Content-Type': 'text/plain' });
        res.end('Faltan datos.');
        return;
      }
  let newEntry = new inventory({
    businessId: req.body.businessId,
    userId: req.body.userId,
    userName: req.body.userName,
    details: req.body.details
  });

  newEntry.save((err, result) => {
    if (err) {
      res.write(err);
    } else {
      req.body.details.forEach((detail, index, array) => {
        actualSupplies.update({_id: detail.insumoId}, {$set: {
          quantity: detail.quantityAfterSave,
          portions: detail.portionsAfterSave
        }}, (err, result) => {
          if (err) {
            return err;
          } else if (index === array.length - 1) {
            res.send('Se ha guardado con éxito.');
          }
        });
      });
    }
  });
});

router.put('/api/inventory/:id', (req, res) => {
  inventory.update({_id: req.params.id}, req.body, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      req.body.details.forEach((detail, index, array) => {
        actualSupplies.update({_id: detail.insumoId}, {$set: {
          quantity: detail.quantityAfterSave,
          portions: detail.portionsAfterSave
          }}, (err, result) => {
          if (err) {
            return err;
          } else if (index === array.length - 1) {
            res.send('Se ha guardado con éxito.');
          }
        });
      });
    }
  });
});

router.post('/api/inventory/shrinkage', (req, res) => {
  let businessArray = [];
  
  
  let fromDate = moment(req.body.date);
  req.body.businessArray.forEach((business) => {
    businessArray.push(business._id);
  });
  
  let toDate   = moment(fromDate).add(1, 'M').subtract(1, "days");
  let fullSupplies = [];
    function enumerateDaysBetweenDates(startDate, endDate) {
    let now = startDate, dates = [];
    
    while (now.isSameOrBefore(endDate)) {
        dates.push(now.format('YYYY/MM/DD'));
        
        now.add(1, 'days');
      }      
      return dates;
    };

    let fromDateISO = new Date(JSON.stringify(fromDate).substring(1,11)  + 'T00:00:00');
    let toDateISO = new Date(JSON.stringify(toDate).substring(1,11)  + 'T00:00:00');
      
  let dates = enumerateDaysBetweenDates(fromDate, toDate);


  if (businessArray.length === 0) {
    res.write('Ha ocurrido un error.');
  } else {
    supplies.find({ businessId: { $in: businessArray } },{name: 1},(err, allSupplies) => {
      if (err) {
        res.write('Ha ocurrido un error.');
        
      } else {
        //fullSupplies = allSupplies;
        if (allSupplies.length === 0) {
          res.send({fullSupplies, dates});
        } else {
          allSupplies.forEach((supply, suppliesindex, suppliesArray) => {
            fullSupplies.push({
              _id: String(supply._id),
              name: supply.name,
              supplies: []
            });
  
            dates.forEach((date, datesIndex, datesArray) => {
              fullSupplies[suppliesindex].supplies.push(0);
    
              if (suppliesindex === suppliesArray.length - 1 && datesIndex === datesArray.length - 1 ) {
                inventory.find({ businessId: { $in: businessArray } , 'date': {$gte: fromDateISO, $lte: toDateISO}}, (err, result) => {
                  if (err) {
                    res.write('Ha ocurrido un error.');
                  } else {
                    let finishArray = [];
                    
                    dates.forEach((date, dateIndex, dateArray) => {
                      if (_.findIndex(finishArray, {date: date}) < 0 ) {
                        finishArray.push({
                          date: date,
                          products: {details: []}
                      });
                      }
                      date = moment(date).format('YYYY/MM/DD');
                      if (result.length === 0) {
                        res.send({fullSupplies, dates});
                      } else {
                        result.forEach((inventory, index, array) => {
                        
                          let inventoryDate = moment(inventory.date).format('YYYY/MM/DD');       
                        
                        
                          if(moment(inventoryDate).diff(date, 'days') === 0) {
                            
                              let currentIndex = -1;
                              
                              finishArray.forEach((array, index) => {
                                  if (finishArray[index].date.indexOf(date) == 0) {
                              currentIndex = index;
                              
                                  }
                              });
                              if (currentIndex >= 0) {
                            inventory.details.forEach((item)=>{
                              let productIndex = _.findIndex(finishArray[currentIndex].products.details, {insumoId: item.insumoId});
                              if ( productIndex >= 0) {
                                finishArray[currentIndex].products.details[productIndex].quantityConsumed += (item.quantityBeforeSave - item.quantityAfterSave);
                              } else {
                                finishArray[currentIndex].products.details.push(item);
                              }
                            });
                            } else {
                                finishArray.push({
                                    date: date,
                                    products: inventory
                                });
                            }
                          }
                          if (dateIndex == dateArray.length - 1 && index == array.length - 1) {
                            finishArray.forEach((day, dayIndex, dayArray) => {
                              if (day.products.details.length > 0) {
                                day.products.details.forEach((supply) => {
                                  let tempIndex = _.findIndex(fullSupplies, {_id: supply.insumoId});
                                  
                                  
                                  if (tempIndex >= 0) {
                                    fullSupplies[tempIndex].supplies[dayIndex] = supply.quantityConsumed;
                                  }
                                });
                              }
                              if (dayIndex === dayArray.length - 1) {
                                res.send({fullSupplies, dates});
                              }
                            });
                          }
                        });
                      }                  
                    });
                    
                  }
                });
              }
            });
          });
        }
      }
    });
  }
});

module.exports = router;
