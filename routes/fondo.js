'use strict';

var express = require('express');
var router = express.Router();
let fondo = require('../models/fondo');
let folios = require('../models/folio');
let internalfolios = require('../models/internal-folio');
let inventories = require('../models/inventory');
let paydesks = require('../models/paydesk');
let salescuts = require('../models/daily-total-ammount');
let ticket = require('../models/tickets');
let suppliesdetails = require('../models/supplies-details');
let rosterdetails = require('../models/roster-details');
let rosterhistories = require('../models/roster-history');




router.post('/api/fondo', function(req, res) {
  let date = new Date();
  let insertFondo = new fondo({
    businessId: req.body.businessId,
    userId: req.body.userId,
    quantity: req.body.quantity,
  });

  insertFondo.save( (err) => {
    if (err) {
      res.write(err);
    } else {
      folios.remove( { date : {"$lt" : new Date(date.getYear() + 1900, date.getMonth() - 3, date.getDay()) } }, (err, result) => {
        if (err) {
          res.write(err);
        } else {
          fondo.remove( { date : {"$lt" : new Date(date.getYear() + 1900, date.getMonth() - 1, date.getDay()) } }, (err, result) => {
            if (err) {
              res.write(err);
            } else {
              internalfolios.remove( { date : {"$lt" : new Date(date.getYear() + 1900, date.getMonth() - 3, date.getDay()) } }, (err, result) => {
                if (err) {
                  res.write(err);
                } else {
                  inventories.remove( { date : {"$lt" : new Date(date.getYear() + 1900, date.getMonth() - 3, date.getDay()) } }, (err, result) => {
                    if (err) {
                      res.write(err);
                    } else {
                      paydesks.remove( { date : {"$lt" : new Date(date.getYear() + 1900, date.getMonth() - 3, date.getDay()) } }, (err, result) => {
                        if (err) {
                          res.write(err);
                        } else {
                          salescuts.remove( { date : {"$lt" : new Date(date.getYear() + 1900, date.getMonth() - 3, date.getDay()) } }, (err, result) => {
                            if (err) {
                              res.write(err);
                            } else {
                              ticket.remove( { date : {"$lt" : new Date(date.getYear() + 1900 - 3, date.getMonth(), date.getDay()) } }, (err, result) => {
                                if (err) {
                                  res.write(err);
                                } else {
                                  suppliesdetails.remove( { date : {"$lt" : new Date(date.getYear() + 1900 - 3, date.getMonth(), date.getDay()) } }, (err, result) => {
                                    if (err) {
                                      res.write(err);
                                    } else {
                                      rosterdetails.remove( { initialDate : {"$lt" : new Date(date.getYear() + 1900, date.getMonth() - 3, date.getDay()) } }, (err, result) => {
                                        if (err) {
                                          res.write(err);
                                        } else {
                                          rosterhistories.remove( { initialDate : {"$lt" : new Date(date.getYear() + 1900, date.getMonth() - 3, date.getDay()) } }, (err, result) => {
                                            if (err) {
                                              res.write(err);
                                            } else {
                                              res.send('Se ha guardado con éxito');
                                            }
                                          });
                                        }
                                      });
                                    }
                                  });
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });          
        }
      });
    }
  });
});

router.get('/api/fondo', (req, res) => {
  fondo.find({salesCut: false}, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send(result);
    }
  });
});

module.exports = router;
