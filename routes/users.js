'use strict';
let express = require('express');
let router = express.Router();

//modelos
let globalUser = require('../models/users');
let rosterDetails = require('../models/roster-details');

router.get('/api/globalUsers', function(req, res) {
 globalUser.find(function(err, result) {
   if (err) {
   } else {
    globalUser.count((err,count) => {
      let users = [];
     
        if (err) {
        } else {
          result.forEach((element) => {
            users.push({
              _id: element._id,
              businessId: element.businessId,
              name: element.name,
              joinDate: element.joinDate,
              rol: element.rol,
              user: element.user
            });
          });   
          res.send({users, count});
        }
    });
   }
  })
  .skip(parseInt(req.query.skip || 0))
  .limit(parseInt(5));
});

router.get('/api/globalUsers/:id', function(req, res) {
  globalUser.find({'_id':req.params.id})
  .then(function (response) {
    res.send([{
      businessId: response[0].businessId,
      name: response[0].name,
      joinDate: response[0].joinDate,
      rol: response[0].rol,
      user: response[0].user
    }]);
  })
  .catch(function() {
    res.writeHead(400, {'Content-Type':'text/plain'});
    res.end('No hay módulos con ese ID');
  });
});

router.get('/api/globalUsers/count', function(req, res) {
  globalUser.count({},function (err, count) {
    if (err) {
      return err;
    }
    res.send({'count': count});
  });
});

router.post('/api/globalUsers',function(req,res) {
  globalUser.find({'user':req.body.user})
  .then((response) => {
    if(response.length >= 1) {
      res.writeHead(400, {'Content-Type': 'text/plain'});
      res.end('Ese nombre ya existe.');
      return;
    } else if (!req.body.user || !req.body.password) {
      res.writeHead(400, {'Content-Type': 'text/plain'});
      res.end('Faltan datos.');
      return;
    }
    let newUser = new globalUser({
      user: req.body.user,
      password: req.body.password,
      rol: req.body.rol,
      name: req.body.name,
      businessId: req.body.businessId
    });
    newUser.save(function (err) {
      if (err) {
        return err;
      }
    })
    .then(function() {
      res.writeHead(200, {'Content-Type': 'text/plain'});
      res.end('Se ha agregado con éxito.');
    });
  })
  .catch((err) => {
    return err;
  });


});

router.put('/api/globalUsers/:id', (req, res) => {
  if (!req.body.user || !req.body.password) {
    res.writeHead(400, {'Content-Type': 'text/plain'});
    res.end('Faltan datos.');
  } else {
    globalUser.update({'_id': req.params.id}, req.body, (err, values) => {
        if (!err) {
            res.json("Se ha actualizado correctamente.");
        } else {
            res.write("Ha ocurrido un error.");
        }
    });
  }
});

router.delete('/api/globalUsers/:id', (req, res) => {
  rosterDetails.find({'userId':req.params.id}, (err, result) => {
    if (err) {
      return err;
    } else if (result.length > 0) {
      rosterDetails.remove({'userId':req.params.id}, (err) => {
        if (err) {
          return err;
        }
      });
    }
  });
  globalUser.remove({'_id': req.params.id},function(err) {
    if (!err) {
        res.json("Se ha actualizado correctamente.");
    } else {
        res.write("Ha ocurrido un error.");
    }
  });
});
module.exports = router;
