'use strict';
let express = require('express');
let router = express.Router();

let business = require('../models/business');
let globalUser = require('../models/users');

router.get('/api/business', (req, res) => {
  business.find((err, result) => {
    if (!err) {
      res.send(result);
    } else {
      res.send(err);
    }
  });
});


router.post('/api/business', (req, res) => {
  business.find((err, result) => {
    if (err) {
    } else {
      //TODO: Cambiar el length por 1.
      if (result.length >= 1) {
        res.writeHead(400, {'Content-Type': 'text/plain'});
        res.end('Acción Denegada.');
        return;
      } else if (!req.body.businessName || !req.body.businessAdress ||
                 !req.body.businessPhone || !req.body.user ||
                 !req.body.password) {
        res.writeHead(400, {'Content-Type': 'text/plain'});
        res.end('Faltan datos.');
        return;
      } else {
        let newBusiness = new business({
          name : req.body.businessName,
          mail : req.body.businessEmail || '',
          address: req.body.businessAdress,
          phone: req.body.businessPhone,
          online: req.body.online
        });
        newBusiness.save((err) => {
          if (err) {
            return err;
          }
        })
        .then( () => {
          let id;
          business.find((err, result) => {
            if (err) {
            } else {
              id = result[0]._id;
            }
          });
          let newUser = new globalUser({
            user: req.body.user,
            password: req.body.password,
            rol: req.body.rol,
            name: req.body.username,
            businessId: id

          });
          newUser.save(function (err) {
            if (err) {
              return err;
            }
          })
          .then( () => {
            res.send({id: id});
          });
        });
      }
    }
  });
});



router.put('/api/business/:id', (req, res) => {
  business.update({ '_id': req.params.id}, {$set: {online: req.body.online}}, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send(result);
    }
  });
});

module.exports = router;
