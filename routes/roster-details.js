'use strict';

let express = require('express');
let router = express.Router();
var _ = require('lodash');

let globalUser = require('../models/users');
let rosterDetails = require('../models/roster-details');
let rosterHistory = require('../models/roster-history');
let rosterId;

router.get('/api/roster', (req, res) => {
  rosterDetails.find((err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
});

router.get('/api/roster/:id', (req, res) => {
  rosterDetails.findOne({'_id':req.params.id}, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
});

router.get('/api/rosters/available-users', (req, res) => {
  rosterDetails.find( (err, result) => {
    if (err) {
      res.send(err);
    } else {
      let currentRosters = result;
      globalUser.find((err, result) => {
        if (err) {
          res.send(err);
        } else {
          let usersToSend = result;
          let temp = [];
          currentRosters.forEach((roster) => {
            temp = [];
            usersToSend.forEach((user, index) => {
              if (roster.userId == user._id) {
                temp.push(index);
              }
            });
            if (temp.length > 0) {
              temp.reverse();
              temp.forEach(function (index) {
                usersToSend.splice(index,1);
              });
            }
          });
          res.send(usersToSend);
        }
      })
    }
  });
});

router.post('/api/roster', (req, res) => {
  rosterDetails.find({'userId': req.body.user})
  .then((response) => {
    if (response.length >= 1) {
      res.writeHead(401, {'Content-Type': 'text/plain'});
      res.end('Ese usuario ya tiene nómina vigente.');
      return;
    }
    let newRoster = new rosterDetails({
      businessId: req.body.businessId,
      userId: req.body.user,
      username: req.body.username,
      payPerDay: req.body.payPerDay,
      payPerExtraHour: req.body.extraHour,
      workedDays: 0,
      extraTime: 0,
      absenceDays: 0,
      total: 0
    });
    newRoster.save((err, result) => {
      if (err) {
        return err;
      }
    })
    .then((response) => {
      res.writeHead(200, {'Content-Type': 'text/plain'});
      res.end('Se ha agregado con éxito.');
    })
    .catch((err) => {
      return err;
    });
  });
});

router.put('/api/roster/:id', (req, res) => {
  rosterDetails.update({"_id": req.params.id}, req.body, (err, values) => {
    if (err) {
      res.write("Ha ocurrido un error.");
    } else {
      res.json('Se ha actualizado correctamente.');
    }
  });
});

router.delete('/api/roster/:id', (req, res) => {
  rosterDetails.remove({"_id":req.params.id}, (err, value) => {
    if (err) {
      res.write('Ha ocurrido un error.');
    } else {
      rosterHistory.remove({'rosterId': req.params.id}, (err, value) => {
        if (err) {
          res.write('Ha ocurrido un error.');
        } else {
          res.json('Se ha eliminado de manera correcta.');
        }
      });
    }
  });
});

module.exports = router;
