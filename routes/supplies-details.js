'use strict';

let express = require('express');
let router = express.Router();
let _ = require('lodash');

let suppliesDetails = require('../models/supplies-details');
let supplies = require('../models/supplies');

router.get('/api/supplies/details', (req, res) => {
  suppliesDetails.find((err, result) => {
    if (err) {
      res.write('Ha ocurrido un error.');
    } else {
      suppliesDetails.count((err,count) => {
        if (err) {
          res.write(err);
        } else {
          result = result.reverse();
          res.send({result,count});          
        }
      });
    }
  })
  .sort([['date', -1]])
  .skip(parseInt(req.query.skip))
  .limit(parseInt(5));
});

router.get('/api/supplies/details/:id', (req, res) => {
  suppliesDetails.findOne({'_id': req.params.id}, (err, result) => {
    if (err) {
      res.write('Ha ocurrido un error.');
    } else {
      res.send(result);
    }
  });
});

router.post('/api/supplies/details', (req, res) => {
  suppliesDetails.findOne({}, {}, { sort: { 'folio' : -1 } }, (err, item) => {
    let count = item && item.folio + 1;
    let newSupplyDetail = new suppliesDetails({
      businessId: req.body.businessId,
      userId: req.body.userId,
      products: req.body.products,
      portions: req.body.portions,
      total: req.body.total,
      folio: count
    });
    
    if (err) {
      res.write('Ha ocurrido un error');
    } else {
      newSupplyDetail.save( (err) => {
        if (err) {
          res.write('Ha ocurrido un error');
        }
      })
      .then(() => {
    
        let actualSuppliesDetails;
        let alreadyInsertedSupplies = [];
    
        supplies.find((err, result) => {
          if (err) {
            return err;
          } else {
            actualSuppliesDetails = result;
    
            req.body.products.forEach((product, index, array) => {
              let pastQuantity = 0;
              let pastPortions = 0;
              if (alreadyInsertedSupplies.length > 0) {
                let sameProducts = _.filter(alreadyInsertedSupplies, { supplyId: product.id });
                sameProducts.forEach((product) => {              
                  pastQuantity += product.quantity || 0;
                  pastPortions += product.portions || 0;
                });
              }
    
              alreadyInsertedSupplies.push({
                supplyId: product.id,
                quantity: product.quantity,
                portions: product.portions
              });
              let portionsPerProduct =_.find(actualSuppliesDetails, { id: product.id}).portionPerProduct;
              
              let quantity = Math.floor((_.find(actualSuppliesDetails, { id: product.id}).portions + product.portions + pastPortions) / portionsPerProduct);
              let portionQuantity = _.find(actualSuppliesDetails, { id: product.id}).portions +
                                    product.portions + pastPortions;
              
                supplies.update({ '_id': product.id}, {$set: {
                quantity: quantity,
                portions: portionQuantity
              } }, (err, values) => {
                if (!err && index === (array.length - 1)) {
                  res.writeHead(200, { 'Content-Type': 'text/plain' });
                  res.end('Se ha guardado con éxito');
                } else {
                  return err;
                }
              });
            });
          }
        });
      })
      .catch(() => {
        res.writeHead(401, { 'Content-Type': 'text/plain' });
        res.end('Se ha producido un error');
      });
    }
  });
});


router.delete('/api/supplies/details/:id', (req, res) => {
  suppliesDetails.findOne({ '_id': req.params.id }, (err, result) => {
    if (err) {
      res.write('Ha ocurrido un error.');
    } else {
      let actualSuppliesDetails;
      supplies.find((err, result2) => {
        if (err) {
          return err;
        } else {
          actualSuppliesDetails = result2;
        }
        let alreadyInsertedSupplies = [];
        actualSuppliesDetails.forEach((supply, index, array) => {
          
          if (!_.find(result.products, { id: supply.id })) {
            alreadyInsertedSupplies.push({
              supplyId: supply.id
            });
          }
          let counter = 0;
          let portions = 0;
          let portionsPerProduct;
          let arrayToCount = _.filter(result.products,{ id: supply.id });
          arrayToCount.forEach((item) => {
            portionsPerProduct = _.find(actualSuppliesDetails, { id: item.id}).portionPerProduct;
            portions += item.portions || 0;
          });
          portions = supply.portions - portions;
          counter = (portions/portionsPerProduct);
          
          supplies.update({ _id: supply.id}, {$set: {
            quantity: counter,
            portions: portions
          } }, (err, values) => {
            if (index === (array.length - 1)) {
              suppliesDetails.remove({ '_id': req.params.id }, (err, result) => {
                if (err) {
                  res.writeHead(400, { 'Content-Type': 'text/plain' });
                  res.end('ha ocurrido un error');
                } else {
                  res.writeHead(200, { 'Content-Type': 'text/plain' });
                  res.end('Se ha guardado con éxito');
                }
              })
            }
          });
        });
      });
    }
  });
});

module.exports = router;
