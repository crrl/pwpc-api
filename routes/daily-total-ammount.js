'use strict';

let express = require('express');
let router = express.Router();
let _ = require('lodash');

let salesCut = require('../models/daily-total-ammount');
let rosterHistory = require('../models/roster-history');
let folio = require('../models/folio');
let folioPending = require('../models/folio-pending');
let internFolio = require('../models/internal-folio');
let suppliesDetails = require('../models/supplies-details');
let globalUser = require('../models/users');
let dailySalesCut = require('../models/daily-sales-cut');
let fondo = require('../models/fondo');
let payDesk = require('../models/paydesk');
let servingPendings = require('../models/serving-pending');
let insertedSalesCut;

router.get('/api/salesCut', (req, res) => {
  dailySalesCut.find((err, result) => {
    if (err) {
      res.write(err);
    } else {
      dailySalesCut.count((err,count) => {
        if (err) {
          res.write(err);
        } else {
          result = result.reverse();
      res.send({result, count});
        }          
      })
    }
  })
  .sort([['date', -1]])
  .skip(parseInt(req.query.skip))
  .limit(parseInt(5));
});

router.get('/api/salesCut/:id', (req, res) => {
  salesCut.find({cutId: req.params.id}, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send(result);
    }
  });
});

router.post('/api/salesCut', (req, res) => {

  let newSalesCut = new salesCut({
    businessId: req.body.businessId,
    userId: req.body.userId,
    userName: req.body.userName,
    products: req.body.products,
    boughts: req.body.boughts,
    entries: req.body.entries,
    Outputs: req.body.Outputs,
    type: req.body.type,
    anotations: req.body.anotations
  });

  newSalesCut.save((err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send('Se ha guardado con éxito');
    }
  });
});

router.post('/api/generateXCut', (req, res) => {
  let totalRoster = 0;
  let resultAcumm = {};
  let users = [];
  let initialDate = req.body.initialDate.substring(0,req.body.initialDate.length - 1);
  let finalDate = req.body.finalDate.substring(0,req.body.finalDate.length - 1);
  rosterHistory.find({'endDate': {'$gte' : initialDate, '$lt': finalDate }})
  .then((result) => {
    resultAcumm.roster = result;
    folio.find({'endDate': {'$gte' : initialDate, '$lt': finalDate }, 'status': 'Activa'})
    .then((result) => {
      resultAcumm.folioHistory = result;
      suppliesDetails.find({'date': {'$gte' : initialDate, '$lt': finalDate }})
      .then((result) => {
        resultAcumm.supplies = result;
        fondo.find({'date': {'$gte' : initialDate, '$lt': finalDate }})
        .then((result) => {
          resultAcumm.fondos = result;
          if (resultAcumm.folioHistory.length > 0) {
            resultAcumm.folioHistory.forEach((folio, currentIndex) => {
              if (folio.userId && _.findIndex(users, {id: folio.userId}) < 0) {
                users.push({
                  id: folio.userId,
                  name: '',
                  rosters: '',
                  supplies: '',
                  folios: '',
                  fondo: ''
                });
              }

              if (!resultAcumm.products) {
                resultAcumm.products = [];
              }

              if (!resultAcumm.folioIds) {
                resultAcumm.folioIds = [];
              }

              if (!resultAcumm.entries) {
                resultAcumm.entries = 0;
              }
              resultAcumm.folioIds.push({
                id: folio._id,
                date: folio.date,
                total: folio.total,
                folio: folio.folio,
                totalTickets: folio.totalTickets
              });

              folio.details.forEach(function (detail) {
                detail.products.forEach(function (product) {
                  resultAcumm.entries += parseFloat(product.quantity) * parseFloat(product.price);
                  var index = _.findIndex(resultAcumm.products, {productId: product.productId});
                  if (index >= 0) {
                    resultAcumm.products[index].quantity =
                    parseFloat(resultAcumm.products[index].quantity) +
                    parseFloat(product.quantity);
                  } else {
                    resultAcumm.products.push({
                      productId: product.productId,
                      productName: product.productName,
                      price: parseFloat(product.price),
                      quantity: parseFloat(product.quantity)
                    });
                  }
                });
              });
            });
          }

          if (resultAcumm.supplies.length > 0) {
            resultAcumm.supplies.forEach((supply) => {
              if (supply.userId && _.findIndex(users, { id: supply.userId }) < 0) {
                users.push({
                  id: supply.userId,
                  rosters: '',
                  supplies: '',
                  folios: '',
                  fondo: ''
                });
              }

              if (!resultAcumm.suppliesIds) {
                resultAcumm.suppliesIds = [];
              }

              if (!resultAcumm.outs) {
                resultAcumm.outs = 0;
              }

              if (!resultAcumm.boughtSupplies) {
                resultAcumm.boughtSupplies = [];
              }

              resultAcumm.outs += parseFloat(supply.total);
              resultAcumm.suppliesIds.push({
                id: supply._id,
                date: supply.date,
                total: supply.total
              });

              supply.products.forEach((product) => {
                var index = _.findIndex(resultAcumm.boughtSupplies, {id: product.originalItemId});
                if (index >= 0) {
                  resultAcumm.boughtSupplies[index].quantity =
                  parseFloat(resultAcumm.boughtSupplies[index].quantity) +
                  parseFloat(product.quantity);

                  resultAcumm.boughtSupplies[index].total +=
                  parseFloat(product.quantity) * parseFloat(product.price);
                } else {
                  resultAcumm.boughtSupplies.push({
                    id:product.originalItemId,
                    OriginalId: product.id,
                    name: product.name,
                    price: product.price,
                    description: product.description,
                    quantity: parseFloat(product.quantity),
                    total: parseFloat(product.quantity) * parseFloat(product.price)
                  });
                }
              });
            });
          }

          if (resultAcumm.roster.length > 0) {
            if (!resultAcumm.rosters) {
              resultAcumm.rosters = 0;
            }
            resultAcumm.roster.forEach((roster) => {
              totalRoster += roster.total || 0;
              resultAcumm.rosters = totalRoster;
              if (roster.userId && _.findIndex(users, {id: roster.userId}) < 0) {
                users.push({
                  id: roster.userId,
                  rosters: '',
                  supplies: '',
                  folios: '',
                  fondo: ''
                });
              }
            });
          }
          let usersRosters = [];
          let usersSupplies = [];
          let usersFolios = [];
          if (users.length > 0 ) {
            users.forEach((user, index) => {
              user.rosters = _.filter(resultAcumm.roster, {userId: user.id});
              user.supplies = _.filter(resultAcumm.supplies, {userId: user.id});
              user.folios = _.filter(resultAcumm.folioHistory, {userId: user.id});
              user.fondo = _.filter(resultAcumm.fondos, {userId: user.id});
            });

            users.forEach((user, index) => {
            if (user.folios.length > 0) {
              user.folios.forEach((folio, currentIndex) => {
                if (!user.products) {
                  user.products = [];
                }
                if (!user.folioIds) {
                  user.folioIds = [];
                }
                if (!user.entries) {
                  user.entries = 0;
                }
                user.folioIds.push({
                  id: folio._id,
                  date: folio.date,
                  total: folio.total
                });
                folio.details.forEach(function (detail) {
                  detail.products.forEach(function (product) {
                    user.entries += parseFloat(product.quantity) * parseFloat(product.price);
                    var index = _.findIndex(user.products, {productId: product.productId});
                    if (index >= 0) {
                      user.products[index].quantity =
                      parseFloat(user.products[index].quantity) +
                      parseFloat(product.quantity);
                    } else {
                      user.products.push({
                        productId: product.productId,
                        productName: product.productName,
                        price: parseFloat(product.price),
                        quantity: parseFloat(product.quantity)
                      });
                    }
                  });
                });
              });
            }
            if (user.supplies.length > 0) {
              user.supplies.forEach((supply) => {
                if (supply.userId && _.findIndex(users, { id: supply.userId }) < 0) {
                  users.push({
                    id: supply.userId,
                    rosters: '',
                    supplies: '',
                    folios: '',
                    fondo: ''
                  });
                }
                if (!user.suppliesIds) {
                  user.suppliesIds = [];
                }

                if (!user.outs) {
                  user.outs = 0;
                }

                if (!user.boughtSupplies) {
                  user.boughtSupplies = [];
                }

                user.outs += parseFloat(supply.total);
                user.suppliesIds.push({
                  id: supply._id,
                  date: supply.date,
                  total: supply.total
                });

                supply.products.forEach((product) => {
                  var index = _.findIndex(user.boughtSupplies, {id: product.originalItemId});
                  if (index >= 0) {
                    user.boughtSupplies[index].quantity =
                    parseFloat(user.boughtSupplies[index].quantity) +
                    parseFloat(product.quantity);

                    user.boughtSupplies[index].total +=
                    parseFloat(product.quantity) * parseFloat(product.price);
                  } else {
                    user.boughtSupplies.push({
                      id:product.originalItemId,
                      OriginalId: product.id,
                      name: product.name,
                      price: product.price,
                      description: product.description,
                      quantity: parseFloat(product.quantity),
                      total: parseFloat(product.quantity) * parseFloat(product.price)
                    });
                  }
                });
              });
            }
          });
          }
          users.forEach((user, index, array) => {
            globalUser.findOne({_id: user.id}, (err, result) => {
              if (err) {
                res.write(err);
              } else if (index < array.length - 1) {
                user.name = result.user;
              } else {
                user.name = result.user;
                res.send({
                  details: resultAcumm,
                  byUsers: users
                });
              }
            });
          });
        })
        .catch((err) => {
          res.write(err);
        });
      })
      .catch((err) => {
        res.write(err);
      });
    })
    .catch((err) => {
      res.write(err);
    });
  })
  .catch((err) => {
    res.write(err);
  });
});

router.post('/api/generateZCut', (req, res) => {
  let totalRoster = 0;
  let resultAcumm = {};
  let users = [];
  folioPending.find((err, pending) => {
    if (err) {
      res.write(err);
    } else {
      if (pending.length > 0) {
        res.writeHead(500, {'Content-Type': 'text/plain'});
        res.end('Favor de finalizar todos los pedidos.');
        return;
      } else {
        rosterHistory.find({salesCut: false})
        .then((result) => {
          resultAcumm.roster = result;
          folio.find({salesCut: false, 'status': 'Activa'})
          .then((result) => {
            resultAcumm.folioHistory = result;
            suppliesDetails.find({salesCut: false})
            .then((result) => {
              resultAcumm.supplies = result;
              fondo.find({salesCut: false})
              .then((result) => {
                servingPendings.remove({}, (err, result) => {});
                resultAcumm.fondos = result;
                if (resultAcumm.folioHistory.length > 0) {
                  resultAcumm.folioHistory.forEach((folio, currentIndex) => {
                    if (folio.userId && _.findIndex(users, {id: folio.userId}) < 0) {
                      users.push({
                        id: folio.userId,
                        rosters: '',
                        supplies: '',
                        folios: '',
                        fondo: ''
                      });
                    }

                    if (!resultAcumm.products) {
                      resultAcumm.products = [];
                    }

                    if (!resultAcumm.folioIds) {
                      resultAcumm.folioIds = [];
                    }

                    if (!resultAcumm.entries) {
                      resultAcumm.entries = 0;
                    }
                    resultAcumm.folioIds.push({
                      id: folio._id,
                      date: folio.date,
                      total: folio.total
                    });

                    folio.details.forEach(function (detail) {
                      detail.products.forEach(function (product) {
                        resultAcumm.entries += parseFloat(product.quantity) * parseFloat(product.price);
                        var index = _.findIndex(resultAcumm.products, {productId: product.productId});
                        if (index >= 0) {
                          resultAcumm.products[index].quantity =
                          parseFloat(resultAcumm.products[index].quantity) +
                          parseFloat(product.quantity);
                        } else {
                          resultAcumm.products.push({
                            productId: product.productId,
                            productName: product.productName,
                            price: parseFloat(product.price),
                            quantity: parseFloat(product.quantity)
                          });
                        }
                      });
                    });
                  });
                }

                if (resultAcumm.supplies.length > 0) {
                  resultAcumm.supplies.forEach((supply) => {
                    if (supply.userId && _.findIndex(users, { id: supply.userId }) < 0) {
                      users.push({
                        id: supply.userId,
                        rosters: '',
                        supplies: '',
                        folios: '',
                        fondo: ''
                      });
                    }

                    if (!resultAcumm.suppliesIds) {
                      resultAcumm.suppliesIds = [];
                    }

                    if (!resultAcumm.outs) {
                      resultAcumm.outs = 0;
                    }

                    if (!resultAcumm.boughtSupplies) {
                      resultAcumm.boughtSupplies = [];
                    }

                    resultAcumm.outs += parseFloat(supply.total);
                    resultAcumm.suppliesIds.push({
                      id: supply._id,
                      date: supply.date,
                      total: supply.total
                    });

                    supply.products.forEach((product) => {
                      var index = _.findIndex(resultAcumm.boughtSupplies, {id: product.originalItemId});
                      if (index >= 0) {
                        resultAcumm.boughtSupplies[index].quantity =
                        parseFloat(resultAcumm.boughtSupplies[index].quantity) +
                        parseFloat(product.quantity);

                        resultAcumm.boughtSupplies[index].total +=
                        parseFloat(product.quantity) * parseFloat(product.price);
                      } else {
                        resultAcumm.boughtSupplies.push({
                          id:product.originalItemId,
                          OriginalId: product.id,
                          name: product.name,
                          description: product.description,
                          price: product.price,
                          description: product.description,
                          quantity: parseFloat(product.quantity),
                          total: parseFloat(product.quantity) * parseFloat(product.price)
                        });
                      }
                    });
                  });
                }

                if (resultAcumm.roster.length > 0) {
                  if (!resultAcumm.rosters) {
                    resultAcumm.rosters = 0;
                  }
                  resultAcumm.roster.forEach((roster) => {
                    totalRoster += roster.total || 0;
                    resultAcumm.rosters += totalRoster;
                    if (roster.userId && _.findIndex(users, {id: roster.userId}) < 0) {
                      users.push({
                        id: roster.userId,
                        rosters: '',
                        supplies: '',
                        folios: '',
                        fondo: ''
                      });
                    }
                  });
                }

                let usersRosters = [];
                let usersSupplies = [];
                let usersFolios = [];
                if (users.length > 0) {
                  users.forEach((user, index) => {
                    user.rosters = _.filter(resultAcumm.roster, {userId: user.id});
                    user.supplies = _.filter(resultAcumm.supplies, {userId: user.id});
                    user.folios = _.filter(resultAcumm.folioHistory, {userId: user.id});
                    user.fondo = _.filter(resultAcumm.fondos, {userId: user.id});
                  });

                  users.forEach((user, index) => {
                  if (user.folios) {
                    user.folios.forEach((folio, currentIndex) => {
                      if (!user.products) {
                        user.products = [];
                      }
                      if (!user.folioIds) {
                        user.folioIds = [];
                      }
                      if (!user.entries) {
                        user.entries = 0;
                      }
                      user.folioIds.push({
                        id: folio._id,
                        date: folio.date,
                        total: folio.total,
                        folio: folio.folio || 0,
                        totalTickets: folio.totalTickets
                      });

                      folio.details.forEach(function (detail) {
                        detail.products.forEach(function (product) {
                          user.entries += parseFloat(product.quantity) * parseFloat(product.price);
                          var index = _.findIndex(user.products, {productId: product.productId});
                          if (index >= 0) {
                            user.products[index].quantity =
                            parseFloat(user.products[index].quantity) +
                            parseFloat(product.quantity);
                          } else {
                            user.products.push({
                              productId: product.productId,
                              productName: product.productName,
                              price: parseFloat(product.price),
                              quantity: parseFloat(product.quantity)
                            });
                          }
                        });
                      });
                    });
                  }

                  if (user.supplies.length > 0) {
                    user.supplies.forEach((supply) => {
                      if (supply.userId && _.findIndex(users, { id: supply.userId }) < 0) {
                        users.push({
                          id: supply.userId,
                          rosters: '',
                          supplies: '',
                          folios: '',
                          fondo: ''
                        });
                      }
                      if (!user.suppliesIds) {
                        user.suppliesIds = [];
                      }

                      if (!user.outs) {
                        user.outs = 0;
                      }

                      if (!user.boughtSupplies) {
                        user.boughtSupplies = [];
                      }

                      user.outs += parseFloat(supply.total);
                      user.suppliesIds.push({
                        id: supply._id,
                        date: supply.date,
                        total: supply.total
                      });

                      supply.products.forEach((product) => {
                        var index = _.findIndex(user.boughtSupplies, {id: product.originalItemId});
                        if (index >= 0) {
                          user.boughtSupplies[index].quantity =
                          parseFloat(user.boughtSupplies[index].quantity) +
                          parseFloat(product.quantity);

                          user.boughtSupplies[index].total +=
                          parseFloat(product.quantity) * parseFloat(product.price);
                        } else {
                          user.boughtSupplies.push({
                            id:product.originalItemId,
                            OriginalId: product.id,
                            name: product.name,
                            price: product.price,
                            description: product.description,
                            quantity: parseFloat(product.quantity),
                            total: parseFloat(product.quantity) * parseFloat(product.price)
                          });
                        }
                      });
                    });
                  }
                });
                }

                if (!resultAcumm.supplies.length > 0 &&
                  !resultAcumm.folioHistory.length > 0 &&
                  !resultAcumm.roster.length > 0) {
                    res.writeHead(401, {'Content-Type': 'text/plain'});
                    res.end('No hay datos necesarios para generar corte Z.');
                    return;
                  }
                  let newTotalSalesCut = new dailySalesCut({
                    businessId: req.body.businessId,
                    userId: req.body.userId,
                    userName: req.body.username,
                    products: resultAcumm.products || [],
                    boughts: resultAcumm.boughtSupplies || [],
                    folios: resultAcumm.folioIds || [],
                    supplies: resultAcumm.suppliesIds || [],
                    fondo: resultAcumm.fondos || [],
                    entries: resultAcumm.entries || 0,
                    Outputs: resultAcumm.outs || 0,
                    rosters: resultAcumm.roster || [],
                    totalRoster: totalRoster,
                    type: req.body.type
                  });

                  newTotalSalesCut.save((err, result) => {
                if (err) {
                  res.write(err);
                } else {
                  var cutId = result._id;
                  if (users.length > 0) {
                    users.forEach((user, index, array) => {
                    globalUser.findOne({'_id': user.id}, (err, result) => {
                      if (err) {
                        res.end(err);
                      } else {
                        user.name = result.name;
                        let newSalesCut = new salesCut({
                          businessId: req.body.businessId,
                          userId: user.id,
                          userName: user.name,
                          products: user.products || [],
                          boughts: user.boughtSupplies || [],
                          folios: user.folioIds || [],
                          supplies: user.suppliesIds || [],
                          entries: user.entries || 0,
                          Outputs: user.outs || 0,
                          fondo: user.fondo || [],
                          rosters: resultAcumm.roster || [],
                          totalRoster: totalRoster,
                          type: req.body.type,
                          cutId: cutId
                        });

                        newSalesCut.save((err, result) => {
                          insertedSalesCut = result;
                          if (err) {
                            res.write(err);
                          }
                        })
                        .then(() => {
                          rosterHistory.update({salesCut: false}, {
                            $set: {
                              salesCut: true,
                              cutId: insertedSalesCut._id
                            }
                          }, { multi : true }, (err, result) => {
                            if (err) {
                              res.write(err);
                            } else {
                              folio.update({salesCut: false}, {
                                $set: {
                                  salesCut: true,
                                  cutId: insertedSalesCut._id
                                }
                              }, { multi : true }, (err, result) => {
                                if (err) {
                                  res.write(err);
                                } else {
                                  suppliesDetails.update({salesCut: false}, {
                                    $set: {
                                      salesCut: true,
                                      cutId: insertedSalesCut._id
                                    }
                                  }, { multi : true }, (err, result) => {
                                    if (err) {
                                      res.write(err);
                                    } else {
                                      internFolio.update({salesCut: false}, {
                                        $set: {
                                          salesCut: true,
                                          cutId: insertedSalesCut._id
                                        }
                                      }, { multi: true }, (err, result) => {
                                        if (err) {
                                          res.write(err);
                                        } else {
                                          fondo.update({salesCut: false}, {
                                            $set: {
                                              salesCut: true,
                                              cutId: insertedSalesCut._id
                                            }
                                          }, { multi: true }, (err, result) => {
                                            if (err) {
                                              res.write(err);
                                            } else if (index === array.length - 1) {
                                              let lastResultItem = 0;
                                              let previousFund = 0;
                                              res.send({
                                                payDesk: result,
                                                details: resultAcumm,
                                                byUsers: users,
                                                insertedSalesCut,
                                                newTotalSalesCut
                                              });
                                            }
                                          });
                                        }
                                      });
                                    }
                                  });
                                }
                              });
                            }
                          })
                        })
                        .catch(() => {
                          res.write('Ha ocurrido un error en la transacción');
                        });
                      }
                    });
                  })
                  }
                }
              })
              });
            })
            .catch((err) => {
              res.write(err);
            });
          })
          .catch((err) => {
            res.write(err);
          });
        })
        .catch((err) => {
          res.write(err);
        });
      }
    }
  });

});




router.post('/api/generateTurnCut', (req, res) => {
  let totalRoster = 0;
  let resultAcumm = {};
  let users = [];
  rosterHistory.find({turnCut: false})
  .then((result) => {
    resultAcumm.roster = result;
    folio.find({'paid': true, 'turnCut': false, 'status': 'Activa'})
    .then((result) => {
      resultAcumm.folioHistory = result;
      suppliesDetails.find({turnCut: false})
      .then((result) => {
        resultAcumm.supplies = result;
        fondo.find({turnCut: false})
        .then((result) => {
          resultAcumm.fondos = result;
          if (resultAcumm.folioHistory.length > 0) {
            resultAcumm.folioHistory.forEach((folio, currentIndex) => {
              if (folio.userId && _.findIndex(users, {id: folio.userId}) < 0) {
                users.push({
                  id: folio.userId,
                  rosters: '',
                  supplies: '',
                  folios: '',
                  fondo: ''
                });
              }

              if (!resultAcumm.products) {
                resultAcumm.products = [];
              }

              if (!resultAcumm.folioIds) {
                resultAcumm.folioIds = [];
              }

              if (!resultAcumm.entries) {
                resultAcumm.entries = 0;
              }
              resultAcumm.folioIds.push({
                id: folio._id,
                date: folio.date,
                total: folio.total
              });

              folio.details.forEach(function (detail) {
                detail.products.forEach(function (product) {
                  resultAcumm.entries += parseFloat(product.quantity) * parseFloat(product.price);
                  var index = _.findIndex(resultAcumm.products, {productId: product.productId});
                  if (index >= 0) {
                    resultAcumm.products[index].quantity =
                    parseFloat(resultAcumm.products[index].quantity) +
                    parseFloat(product.quantity);
                  } else {
                    resultAcumm.products.push({
                      productId: product.productId,
                      productName: product.productName,
                      price: parseFloat(product.price),
                      quantity: parseFloat(product.quantity)
                    });
                  }
                });
              });
            });
          }

          if (resultAcumm.supplies.length > 0) {
            resultAcumm.supplies.forEach((supply) => {
              if (supply.userId && _.findIndex(users, { id: supply.userId }) < 0) {
                users.push({
                  id: supply.userId,
                  rosters: '',
                  supplies: '',
                  folios: '',
                  fondo: ''
                });
              }

              if (!resultAcumm.suppliesIds) {
                resultAcumm.suppliesIds = [];
              }

              if (!resultAcumm.outs) {
                resultAcumm.outs = 0;
              }

              if (!resultAcumm.boughtSupplies) {
                resultAcumm.boughtSupplies = [];
              }

              resultAcumm.outs += parseFloat(supply.total);
              resultAcumm.suppliesIds.push({
                id: supply._id,
                date: supply.date,
                total: supply.total
              });

              supply.products.forEach((product) => {
                var index = _.findIndex(resultAcumm.boughtSupplies, {id: product.originalItemId});
                if (index >= 0) {
                  resultAcumm.boughtSupplies[index].quantity =
                  parseFloat(resultAcumm.boughtSupplies[index].quantity) +
                  parseFloat(product.quantity);

                  resultAcumm.boughtSupplies[index].total +=
                  parseFloat(product.quantity) * parseFloat(product.price);
                } else {
                  resultAcumm.boughtSupplies.push({
                    id:product.originalItemId,
                    OriginalId: product.id,
                    name: product.name,
                    description: product.description,
                    price: product.price,
                    description: product.description,
                    quantity: parseFloat(product.quantity),
                    total: parseFloat(product.quantity) * parseFloat(product.price)
                  });
                }
              });
            });
          }

          if (resultAcumm.roster.length > 0) {
            if (!resultAcumm.rosters) {
              resultAcumm.rosters = 0;
            }
            resultAcumm.roster.forEach((roster) => {
              totalRoster += roster.total || 0;
              resultAcumm.rosters += totalRoster;
              if (roster.userId && _.findIndex(users, {id: roster.userId}) < 0) {
                users.push({
                  id: roster.userId,
                  rosters: '',
                  supplies: '',
                  folios: '',
                  fondo: ''
                });
              }
            });
          }

          let usersRosters = [];
          let usersSupplies = [];
          let usersFolios = [];
          if (users.length > 0) {
            users.forEach((user, index) => {
              user.rosters = _.filter(resultAcumm.roster, {userId: user.id});
              user.supplies = _.filter(resultAcumm.supplies, {userId: user.id});
              user.folios = _.filter(resultAcumm.folioHistory, {userId: user.id});
              user.fondo = _.filter(resultAcumm.fondos, {userId: user.id});
            });

            users.forEach((user, index) => {
            if (user.folios) {
              user.folios.forEach((folio, currentIndex) => {
                if (!user.products) {
                  user.products = [];
                }
                if (!user.folioIds) {
                  user.folioIds = [];
                }
                if (!user.entries) {
                  user.entries = 0;
                }
                user.folioIds.push({
                  id: folio._id,
                  date: folio.date,
                  total: folio.total,
                  folio: folio.folio || 0,
                  totalTickets: folio.totalTickets
                });

                folio.details.forEach(function (detail) {
                  detail.products.forEach(function (product) {
                    user.entries += parseFloat(product.quantity) * parseFloat(product.price);
                    var index = _.findIndex(user.products, {productId: product.productId});
                    if (index >= 0) {
                      user.products[index].quantity =
                      parseFloat(user.products[index].quantity) +
                      parseFloat(product.quantity);
                    } else {
                      user.products.push({
                        productId: product.productId,
                        productName: product.productName,
                        price: parseFloat(product.price),
                        quantity: parseFloat(product.quantity)
                      });
                    }
                  });
                });
              });
            }

            if (user.supplies.length > 0) {
              user.supplies.forEach((supply) => {
                if (supply.userId && _.findIndex(users, { id: supply.userId }) < 0) {
                  users.push({
                    id: supply.userId,
                    rosters: '',
                    supplies: '',
                    folios: '',
                    fondo: ''
                  });
                }
                if (!user.suppliesIds) {
                  user.suppliesIds = [];
                }

                if (!user.outs) {
                  user.outs = 0;
                }

                if (!user.boughtSupplies) {
                  user.boughtSupplies = [];
                }

                user.outs += parseFloat(supply.total);
                user.suppliesIds.push({
                  id: supply._id,
                  date: supply.date,
                  total: supply.total
                });

                supply.products.forEach((product) => {
                  var index = _.findIndex(user.boughtSupplies, {id: product.originalItemId});
                  if (index >= 0) {
                    user.boughtSupplies[index].quantity =
                    parseFloat(user.boughtSupplies[index].quantity) +
                    parseFloat(product.quantity);

                    user.boughtSupplies[index].total +=
                    parseFloat(product.quantity) * parseFloat(product.price);
                  } else {
                    user.boughtSupplies.push({
                      id:product.originalItemId,
                      OriginalId: product.id,
                      name: product.name,
                      price: product.price,
                      description: product.description,
                      quantity: parseFloat(product.quantity),
                      total: parseFloat(product.quantity) * parseFloat(product.price)
                    });
                  }
                });
              });
            }
          });
          }

          if (!resultAcumm.supplies.length > 0 &&
            !resultAcumm.folioHistory.length > 0 &&
            !resultAcumm.roster.length > 0) {
              res.writeHead(401, {'Content-Type': 'text/plain'});
              res.end('No hay datos necesarios para generar corte Z.');
              return;
            }


            let newTotalSalesCut = new dailySalesCut({
              businessId: req.body.businessId,
              userId: req.body.userId,
              userName: req.body.username,
              products: resultAcumm.products || [],
              boughts: resultAcumm.boughtSupplies || [],
              folios: resultAcumm.folioIds || [],
              supplies: resultAcumm.suppliesIds || [],
              fondo: resultAcumm.fondos || [],
              entries: resultAcumm.entries || 0,
              Outputs: resultAcumm.outs || 0,
              rosters: resultAcumm.roster || [],
              totalRoster: totalRoster,
              type: 'De Turno',
            });

            newTotalSalesCut.save((err, result) => {
          if (err) {
            res.write(err);
          } else {
            var cutId = result._id;
            if (users.length > 0) {
              users.forEach((user, index, array) => {
              globalUser.findOne({'_id': user.id}, (err, result) => {
                if (err) {
                  res.end(err);
                } else {
                  user.name = result.name;
                  let newSalesCut = new salesCut({
                    businessId: req.body.businessId,
                    userId: user.id,
                    userName: user.name,
                    products: user.products || [],
                    boughts: user.boughtSupplies || [],
                    folios: user.folioIds || [],
                    supplies: user.suppliesIds || [],
                    entries: user.entries || 0,
                    Outputs: user.outs || 0,
                    fondo: user.fondo || [],
                    rosters: resultAcumm.roster || [],
                    totalRoster: totalRoster,
                    type: 'De Turno',
                    cutId: cutId
                  });

                  newSalesCut.save((err, result) => {
                    insertedSalesCut = result;
                    if (err) {
                      res.write(err);
                    }
                  })
                  .then(() => {
                    rosterHistory.update({turnCut: false}, {
                      $set: {
                        turnCut: true
                      }
                    }, { multi : true }, (err, result) => {
                      if (err) {
                        res.write(err);
                      } else {
                        folio.update({paid: true, turnCut: false}, {
                          $set: {
                            turnCut: true
                          }
                        }, { multi : true }, (err, result) => {
                          if (err) {
                            res.write(err);
                          } else {
                            suppliesDetails.update({turnCut: false}, {
                              $set: {
                                turnCut: true
                              }
                            }, { multi : true }, (err, result) => {
                              if (err) {
                                res.write(err);
                              } else {
                                internFolio.update({turnCut: false}, {
                                  $set: {
                                    turnCut: true
                                  }
                                }, { multi: true }, (err, result) => {
                                  if (err) {
                                    res.write(err);
                                  } else {
                                    fondo.update({turnCut: false}, {
                                      $set: {
                                        turnCut: true
                                      }
                                    }, { multi: true }, (err, result) => {
                                      if (err) {
                                        res.write(err);
                                      } else if (index === array.length - 1) {
                                        let lastResultItem = 0;
                                        let previousFund = 0;
                                        payDesk.find((err, result) => {
                                          if (err) {
                                            res.write(err);
                                          } else {
                                            let totalFund = 0;
                                            resultAcumm.fondos.forEach((fondo) => {
                                              totalFund += fondo.quantity;
                                            })

                                            lastResultItem = (result[result.length - 1] && result[result.length - 1].fundAvailable || 0) +
                                            (resultAcumm.entries || 0) -
                                            (resultAcumm.outs || 0) -
                                            (totalRoster || 0);

                                            previousFund = (result[result.length - 1] && result[result.length - 1].fundAvailable || 0);

                                            let savePayDesk = new payDesk({
                                              fundAvailable : lastResultItem || 0,
                                              previousFund: previousFund,
                                              withdrawal: 0,
                                              deposit: 0,
                                              sells: resultAcumm.entries,
                                              compras: resultAcumm.outs,
                                              rosters: totalRoster,
                                              cashFund: totalFund
                                            });

                                            savePayDesk.save((err) => {
                                              if (err) {
                                                res.write(err);
                                              }
                                            })
                                            .then(() => {
                                              res.send({
                                                payDesk: result,
                                                details: resultAcumm,
                                                byUsers: users
                                              });
                                            });
                                          }
                                        })
                                      }
                                    });
                                  }
                                });
                              }
                            });
                          }
                        });
                      }
                    })
                  })
                  .catch(() => {
                    res.write('Ha ocurrido un error en la transacción');
                  });
                }
              });
            })
            }
          }
        })
        });
      })
      .catch((err) => {
        res.write(err);
      });
    })
    .catch((err) => {
      res.write(err);
    });
  })
  .catch((err) => {
    res.write(err);
  });
});

router.put('/api/updateZCut', (req, res) => {
  dailySalesCut.update({_id: req.body.cutId}, {
    $set: {
      online: true
    }
  }, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      salesCut.update({_id: req.body._id}, {
        $set: {
          online: true
        }
      }, (err, result) => {
        if (err) {
          res.write(err);
        } else {
          res.send('Exito');
        }
      });
    }
  });
});

module.exports = router;
