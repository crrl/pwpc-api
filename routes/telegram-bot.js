const express = require('express');
const router = express.Router();
const moment = require('moment');

const TelegramBot = require('node-telegram-bot-api');
const fondo = require('../models/fondo');
const business = require('../models/business');
const actualSupplies = require('../models/actualSupplies');
const folio = require('../models/folio');
const suppliesDetails = require('../models/supplies-details');
ticket = require('../models/tickets');

//TOKEN PARA taqueriAPI -- Modificar la siguiente linea para apuntar a otro bot.
const token = '614937069:AAFI2M_eIj2pdlhLTENeq-L4u3LDXGwmVWY';
const chatId = 508469305;
const bot = new TelegramBot(token, {polling: true});

let command;
let arrayOfValues = [];
let options = {};

bot.onText(/\/start/, (msg) => {
  bot.sendMessage(msg.chat.id, 'Comandos: \n' + 
        'insumos: Muestra listado de insumos por cada negocio. \n' +
        'fondos: Muestra un listado de fondos activos. \n' +
        'tickets: Muestra un listado de los últimos 50 tickets. \n' +
        'ticket #: Muestra más detalles sobre un ticket especificado. \n' +
        'detalleCompras: Muestra los últimos 15 detalles de las compras de insumos.\n' +
        'compra #: Muestra detalle sobre compra seleccionada.');  
});

let weekday = new Array(7);
weekday[0] = 'Domingo';
weekday[1] = 'Lunes';
weekday[2] = 'Martes';
weekday[3] = 'Miercoles';
weekday[4] = 'Jueves';
weekday[5] = 'Viernes';
weekday[6] = 'Sábado';


    
const showFondos = () => {
  fondo.find({salesCut: false}, (err, result) => {
    if (err) {
      bot.sendMessage(chatId,err);
    } else {
      business.find((err,business) => {
        if (err) {
          bot.sendMessage(chatId,err);
        } else {
          let businessName = business[0].name;
          let resultToSend = '**' + businessName + '**' + '\n';
          resultToSend += '-------------\n';
          result.forEach((fondo, index, array) => {
            resultToSend += 'Cantidad: ' + fondo.quantity + '\n\n';
            if (index == array.length-1) {
              bot.sendMessage(chatId, String(resultToSend));
            }
          });
        }
      })
    }
  });
};

const showSupplies = () => {
  business.find((err,business) => {
    if (err) {
      bot.sendMessage(chatId,err);
    } else {
      let businessName = business[0].name;
      let counter = 0;

      
      actualSupplies.count((err,count) => {
        counter = count || 0;
        counter = Math.ceil(counter / 10);
        if (err) {
          bot.sendMessage(chatId,err);
        } else {
          for(let i = 0; i < counter; i++) {
            actualSupplies.find((err, result) => {
              if (err) {
                bot.sendMessage(chatId,err);
              } else {
                let resultToSend = '**' + businessName + '**' + '\n';
                resultToSend += '-------------\n';
                result.forEach((insumo, index, array) => {
                  resultToSend += 'Nombre: ' + insumo.name + '\nPresentación: ' + insumo.typeDescription + '\nCantidad: ' + insumo.quantity + '\n\n'; 
                  if (index == array.length-1) {
                    bot.sendMessage(chatId, String(resultToSend));
                  }
                })
              }
            })
            .sort([['date', -1]])
            .skip(parseInt(i*10))
            .limit(parseInt(10)); 
          }
        }          
      }) 
    }
  });
};

const showTickets = () => {
  business.find((err, business) => {
    if (err) {
      bot.sendMessage(chatId,err);
    } else {
      let businessName = business[0].name;

      let counter = 0;

    
      ticket.count((err,count) => {
        counter = count || 0;
        if (counter > 50) {
          counter = 50;
        }
        counter = Math.ceil(counter / 10);
        if (err) {
          bot.sendMessage(chatId,err);
        } else {
          for(let i = 0; i < counter; i++) {
            ticket.find((err,result) => {
              if (err) {
                bot.sendMessage(chatId,err);
              } else {
                let resultToSend = '**' + businessName + '**' + '\n';
                resultToSend += '-------------\n';
                result.forEach((ticket, index, array) => {
                  resultToSend += 'Fecha: ' + moment(ticket.date).format('DD-MM-YYYY') + '\nEncargado: ' + ticket.userName + '\nTotal: $' + ticket.total.toFixed(2) + '\nInicio: ' + ticket.initialHour +
                  ' Final: '+ ticket.endHour + '\nFolio: ' + ticket.folioNumber + '\n\n'; 
                  if (index == array.length-1) {
                    bot.sendMessage(chatId, String(resultToSend));
                  }
                })
              }
            })
            .sort([['date', -1]])
            .skip(parseInt(i*10))
            .limit(parseInt(10)); 
          }
        }
      });
    }
  });
};

const showBuyDetails = () => {
  business.find((err, business) => {
    if (err) {
      bot.sendMessage(chatId,err);
    } else {
      let businessName = business[0].name;
      suppliesDetails.find((err, result) => {
        if (err) {
          bot.sendMessage(chatId,err);
        } else {
          let resultToSend = '**' + businessName + '**' + '\n';
          resultToSend += '-------------\n';
          result.forEach((supply, index, array) => {
            resultToSend += 'Fecha: ' + moment(supply.date).format('DD-MM-YYYY');
            resultToSend += '   -    Folio: ' + supply.folio;                
            resultToSend += '\nTotal gastado: $' + supply.total.toFixed(2);
            resultToSend += '\nDía: ' + weekday[supply.dayNumber];
            resultToSend += '\nCorte de turno: ' + (supply.turnCut ? 'Finalizado' : 'En espera');
            resultToSend += '\nCorte Z: ' + (supply.salesCut ? 'Finalizado' : 'En espera') + '\n'; 
            resultToSend += '\n\n\n';
            if (index == array.length - 1) {
              bot.sendMessage(chatId, String(resultToSend));
            }
          });
        }
      })
      .sort([['date', -1]])
      .limit(parseInt(15));
    }
  });
};

const showSelectedTicket = () => {
  business.find((err, business) => {
    if (err) {
      bot.sendMessage(chatId,err);
    } else {
      let businessName = business[0].name;
      
      if (!arrayOfValues || arrayOfValues.length < 1) {
        arrayOfValues = [];
        bot.sendMessage(chatId, 'No se encontró el folio especificado.');
        return;
      }

      ticket.find({'folio': arrayOfValues[0]}, (err,result) => { 
                   
        if (err || result.length === 0 ) {
          arrayOfValues = [];
          bot.sendMessage(chatId, 'No se encontró el folio especificado.');
          } else {
          let resultToSend = '**' + businessName + '**' + '\n';
          resultToSend += '-------------\n';
          result.forEach((ticket, index, array) => {
            resultToSend += 'Fecha: ' + moment(ticket.date).format('DD-MM-YYYY') + ' - Encargado: ' + ticket.userName + ' total: $' + ticket.total.toFixed(2) + ' Inicio: ' + ticket.initialHour +
            ' Final: '+ ticket.endHour + '\n'; 
            if (index == array.length-1) {
              folio.find({'folio': arrayOfValues[0]}, (err,folio) => {
                if (err || !folio || folio.length === 0 ) {
                  arrayOfValues = [];
                bot.sendMessage(chatId, 'No se encontró el folio especificado.');
                } else {
                  resultToSend += '---------------\n';
                  resultToSend += ' Día: ' + weekday[folio[0].dayNumber];                      
                  resultToSend += ' - Clientes: ' + folio[0].details.length;
                  resultToSend += ' - Mesa: ' + folio[0].table;
                  resultToSend += ' - Estado: ' + folio[0].status;
                  resultToSend += ' - Tickets: ' + folio[0].totalTickets;
                  resultToSend += ' - Pagada: ' + (folio[0].paid ? 'Si' : 'No');
                  resultToSend += ' - Corte de turno: ' + (folio[0].turnCut ? 'Finalizado' : 'En espera');
                  resultToSend += ' - Corte Z: ' + (folio[0].salesCut ? 'Finalizado' : 'En espera'); 
                  arrayOfValues = [];
                  bot.sendMessage(chatId, String(resultToSend));
                }
              });
            }
          });
        }
      });
    }
  }); 
};

const showBuy = () => {
  business.find((err, business) => {
    if (err) {
      bot.sendMessage(chatId,err);
    } else {
      let businessName = business[0].name;

      if (!arrayOfValues || arrayOfValues.length < 1) {
        arrayOfValues = [];
        bot.sendMessage(chatId, 'No se encontró el folio especificado.');
        return;
      }

      suppliesDetails.findOne({'folio': arrayOfValues[0]}, (err,result) => {          
        if (err || !result) {
          bot.sendMessage(chatId, 'No se encontró el folio especificado.');
          } else {
          let resultToSend = '**' + businessName + '**' + '\n';
          resultToSend += 'Fecha: ' + moment(result.date).format('DD-MM-YYYY');
          resultToSend += '-------------\n';
          result.products.forEach((supply, index, array) => {
            resultToSend += '\nProducto: ' + supply.name;
            resultToSend += '\nCantidad: ' + supply.quantity;
            resultToSend += '\nPrecio: $' + supply.price.toFixed(2);
            resultToSend += '\nTotal: $'+ supply.total.toFixed(2) + '\n'; 
            if (index == array.length-1) {
              bot.sendMessage(chatId, String(resultToSend));
            }
          })
        }
      });
    }
  });    
};

const showOptions = () => {
  options = {
    reply_markup: JSON.stringify({
      inline_keyboard: [
        [{ text: 'Insumos', callback_data: 'insumos' }],
        [{ text: 'Fondos', callback_data: 'fondos' }],
        [{ text: 'Tickets', callback_data: 'tickets' }],
        [{ text: 'Detalle de compras', callback_data: 'detalleCompras' }]
      ]
    })
  };
  
  bot.sendMessage(chatId, 'Comandos: \n' + 
  'insumos: Muestra listado de insumos por cada negocio. \n' +
  'fondos: Muestra un listado de fondos activos. \n' +
  'tickets: Muestra un listado de los últimos 50 tickets. \n' +
  'detalleCompras: Muestra los últimos 15 detalles de las compras de insumos.\n', options);
};

const checkMessage = (msg) => {
  switch (msg.toString().toLowerCase()) {
    case 'fondos':
      showFondos();
    break;

    case 'insumos': 
      showSupplies();
    break;

    case 'tickets': 
      showTickets();
    break;

    case 'detallecompras': 
      showBuyDetails();
    break;

    case 'ticket': 
      showSelectedTicket();
    break;

    case 'compra': 
      showBuy();
    break;

    case 'menu':
      showOptions();
    break;

    default: 
      bot.sendMessage(chatId, 'Comandos: \n' + 
        'menu: Muestra un listado de comandos predeterminados. (ej: menu) \n' +
        'ticket #: Muestra más detalles sobre un ticket especificado. (ej: ticket 30) \n' +
        'compra #: Muestra detalle sobre compra seleccionada. (ej: compra 1)'); 
    break;
  }
};

bot.on('message', (msg) => {
  if (msg.chat.id !== chatId) {
    return;
  }
  
  if (msg.text.indexOf(' ') > -1) {
    arrayOfValues = msg.text.split(' ').splice(1);
    msg.text = msg.text.split(' ')[0];
  }

  checkMessage(msg.text);
      
});

bot.on('callback_query', function (msg) {
  checkMessage(msg.data)
});

/*router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
*/

module.exports = router;
