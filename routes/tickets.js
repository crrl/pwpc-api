'use strict';
let express = require('express');
let router = express.Router();
let ticket = require('../models/tickets');
let internFolio = require('../models/internal-folio');
let folio = require('../models/folio');
let supplies = require('../models/supplies');
let _ = require('lodash');

router.post('/api/ticket', (req, res) => {
  let portionsToSave = [];
  let hasPortions = false;
  let ticketsInFolio = 0;
  ticket.find({'folio': req.body.folio}, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      if (req.body.internalFood) {
        internFolio.find({'folio': req.body.folio}, (err, folioFound) => {
          if (err) {
          } else {
            if (folioFound.length > 0) {
              ticketsInFolio = folioFound[0].totalTickets + 1;
              internFolio.update({'folio': req.body.folio}, {$set: {totalTickets: ticketsInFolio}},(err, updated) => {
                if (err) {
                  res.write(err);
                } else {
                  if (result.length === 0) {
                    let newTicket = new ticket({
                      businessId: req.body.businessId,
                      userId: req.body.userId,
                      userName: req.body.userName,
                      total: req.body.total,
                      table: req.body.table,
                      folio: req.body.folio,
                      folioNumber: req.body.folio,
                      initialHour: req.body.initialHour,
                      endHour: req.body.endHour,
                      totalTime: req.body.totalTime,
                      products: req.body.products,
                    });
                   
                    newTicket.save((err, result) => {
                      if (err) {
                        res.write(err);
                      } else {
                        res.send(result);
                      }
                    });
                  } else {
                    let newTicket = new ticket({
                      businessId: req.body.businessId,
                      userId: req.body.userId,
                      userName: req.body.userName,
                      total: req.body.total,
                      table: req.body.table,
                      folio: 'RE' + req.body.folio,
                      folioNumber: req.body.folio,
                      initialHour: req.body.initialHour,
                      endHour: req.body.endHour,
                      totalTime: req.body.totalTime,
                      products: req.body.products,
                    });

                    newTicket.save((err, result) => {
                      if (err) {
                        res.write(err);
                      } else {
                        res.send(result);
                      }
                    });
                  }
                }
              });
            }
          }
        });
      } else {
        folio.find({'folio': req.body.folio}, (err, folioFound) => {
          if (err) {
          } else {
            if (folioFound.length > 0) {
              ticketsInFolio = folioFound[0].totalTickets + 1;
              folio.update({'folio': req.body.folio}, {$set: {totalTickets: ticketsInFolio}},(err, updated) => {
                if (err) {
                  res.write(err);
                } else {
                  if (result.length === 0) {
                    let newTicket = new ticket({
                      businessId: req.body.businessId,
                      userId: req.body.userId,
                      userName: req.body.userName,
                      total: req.body.total,
                      table: req.body.table,
                      folio: req.body.folio,
                      folioNumber: req.body.folio,
                      initialHour: req.body.initialHour,
                      endHour: req.body.endHour,
                      totalTime: req.body.totalTime,
                      products: req.body.products,
                    });

                    req.body.products.forEach((product, index, array) => {
                      if (product.portions && product.portions.length > 0)  {
                        hasPortions = true;
                        let productQuantity = product.quantity;
                        product.portions.forEach((portion) => {
                          let portionsToSaveIndex = -1;
                          portionsToSaveIndex = _.findIndex(portionsToSave, {id: portion.id});
                          if (portionsToSaveIndex < 0) {
                            portionsToSave.push({
                              id: portion.id,
                              usedPortions: portion.quantity * productQuantity
                            });
                          } else {
                            portionsToSave[portionsToSaveIndex].usedPortions += portion.quantity * productQuantity;
                          }
                        });
                      }
                    });
                    if (hasPortions) {
                      portionsToSave.forEach((savePortion,portionIndex, portionArray) => {
                        let portions = 0;
                        let quantityLeft = 0;
                        
                        supplies.findOne({_id: savePortion.id}, (err, result) => {
                          if (err) {
                            res.write(err);
                          } else {
                            portions = result.portions - savePortion.usedPortions;
                            quantityLeft = portions / result.portionPerProduct;
                            supplies.update({ _id: savePortion.id }, {$set: {
                              portions: portions,
                              quantity: quantityLeft
                            }}, (err, result) => {
                              if (err) {
                                return err;
                              } else {
                              if (portionIndex === portionArray.length - 1) {
                                newTicket.save((err, result) => {
                                  if (err) {
                                    res.write(err);
                                  } else {
                                    res.send(result);
                                  }
                                });
                              }
                              }
                            })  
                          }
                        });
                      });
                    } else {
                      newTicket.save((err, result) => {
                        if (err) {
                          res.write(err);
                        } else {
                          res.send(result);
                        }
                      });
                    }
                  } else {
                    let newTicket = new ticket({
                      businessId: req.body.businessId,
                      userId: req.body.userId,
                      userName: req.body.userName,
                      total: req.body.total,
                      table: req.body.table,
                      folio: 'RE' + req.body.folio,
                      folioNumber: req.body.folio,
                      initialHour: req.body.initialHour,
                      endHour: req.body.endHour,
                      totalTime: req.body.totalTime,
                      products: req.body.products,
                    });

                    newTicket.save((err, result) => {
                      if (err) {
                        res.write(err);
                      } else {
                        res.send(result);
                      }
                    });
                  }
                }
              });
            }
          }
        });
      }
    }
  });
});

router.get('/api/ticket/:folio', (req, res) => {
  ticket.find({'folioNumber': req.params.folio}, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send(result);
    }
  });
});

module.exports = router;
