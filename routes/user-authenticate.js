'use strict';

var jwt    = require('jsonwebtoken');
var express = require('express');
var router = express.Router();
var app = require('../app');

var Business = require('../models/business');
var globalUser = require('../models/users');
let credentials = require('../models/credentials');

router.post('/api/authenticate', function(req, res) {

  Business.find((err, result) => {
    if (err) {
      res.write(err);
    } else {
      let businessId = result[0]._id;
      credentials.findOne({ businessId: businessId }, (err, result) => {
        if (err) {
          res.write(err);
        } else {
          let regex;

          let array = ['zuyq','qlkt','mzxl','nopt','rwcx','ivxc','xlzb','juio','ikda','tiop','gxvn'];
          let numbers = ['0','1','2','3','4','5','6','7','8','9','-'];
          let endDateArray = result.token;
          let searchString = '';


          array.forEach((item, index) => {
            regex = new RegExp(item, "g");
            endDateArray = endDateArray.replace(regex, numbers[index]);
          });

          let currentDate = new Date();
          let endDate = new Date(endDateArray);
          if (currentDate.getTime() > endDate.getTime()) {
            res.status(500);
            res.end('Fecha limite de pago excedida');
            return;
          }
          globalUser.findOne({
            user: req.body.user
          }, function (err, user) {
            if (err) {
              throw err;
            }
            if (!user) {
              res.status(404);
              res.end('No hay usuarios con ese Nombre/Contraseña');
            } else if (user) {
              let tempPass = Buffer.from(req.body.password, 'base64').toString();
              if (user.password !== tempPass) {
                res.status(404);
                res.end('No hay usuarios con ese Nombre/Contraseña');
              } else {
                var token = jwt.sign(user, app.get('secretKey'), {
                  expiresIn: 31536000
                });
                res.send({
                  token,
                  _id: user._id,
                  username: user.name,
                  business: user.businessId
                });
              }
            }
          });
        }
      })
    }
  });

});

router.get('/api/authenticate/:id', function(req, res) {
  credentials.findOne({ businessId: req.params.id }, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      let regex;

      let array = ['zuyq','qlkt','mzxl','nopt','rwcx','ivxc','xlzb','juio','ikda','tiop','gxvn'];
      let numbers = ['0','1','2','3','4','5','6','7','8','9','-'];
      let endDateArray = result.token;
      let searchString = '';


      array.forEach((item, index) => {
        regex = new RegExp(item, "g");
        endDateArray = endDateArray.replace(regex, numbers[index]);
      });

      let currentDate = new Date();
      let endDate = new Date(endDateArray);
      
      res.send({ date: endDateArray.substring(0,10) });
    }
  })
});


router.put('/api/authenticate', function(req, res) {
  let serial = req.body.serial.split('ese')[0];
  let confirmCode = req.body.serial.split('ese')[1];
  if (!confirmCode) {
    res.status(404);
    res.end('No hay usuarios con ese Nombre/Contraseña');
    return;
  }
  serial = serial.split("e").join('').split("f").join('').split("h").join('').split("s").join('').split('-').join('');
  confirmCode = confirmCode.split("e").join('').split("f").join('').split("h").join('').split("s").join('').split('-').join('');
  let array = ['zuyq','qlkt','mzxl','nopt','rwcx','ivxc','xlzb','juio','ikda','tiop','gxvn']; //efhs
  let numbers = ['0','1','2','3','4','5','6','7','8','9','-'];
  let endDateArray = serial;
  
  let regex;
  array.forEach((item, index) => {
    regex = new RegExp(item, "g");
    endDateArray = endDateArray.replace(regex, numbers[index]);
    confirmCode = confirmCode.replace(regex, numbers[index]);
  });
  
  let currentDate = new Date();
  let endDate = new Date(endDateArray);
  
  if (currentDate.getTime() < endDate.getTime() && !isNaN(confirmCode)) {
    Business.findOne({ autogen: confirmCode }, (err, business) => {
      if (err) {
        res.write(err);
      } else {
        credentials.findOne({ 'businessId': req.body.id}, (err, credential) => {
          if (!credential) {
            let newCredential = new credentials({
              businessId: req.body.id, 
              token: serial,
              online: true
            });
            newCredential.save(function (err) {
              if (err) {
                res.write(err);
              } else {
                res.send('Resultado exitoso.');
              }
            });
          } else {            
            credential.token = serial;
            credential.online = true;
            credential.save( (err, newCredential) => {
              if (err) {
                res.write(err);
              } else {
                business.autogen = Math.floor(Math.random() * 1000000);
                business.save( (err, newBusiness) => {
                  if (err) {
                    res.write(err);
                  } else {
                    res.send('Resultado exitoso.');                  
                  }
                })
              }
            });
          }
        });
      }
    });
  } else {
    res.status(404);
    res.end('No hay usuarios con ese Nombre/Contraseña');
  }
});


router.post('/api/pwpc/business', (req, res) => {
  if (!req.body.businessName || !req.body.businessAdress || !req.body.businessId ||
              !req.body.businessPhone || !req.body.user ||
              !req.body.password) {
    res.writeHead(400, {'Content-Type': 'text/plain'});
    res.end('Faltan datos.');
    return;
  } else {
    let newBusiness = new Business({
      businessId: req.body.businessId,
      name: req.body.businessName,
      mail: req.body.businessEmail || '',
      address: req.body.businessAdress,
      phone: req.body.businessPhone
    });

    newBusiness.save((err) => {
      if (err) {
        return err;
      }
    })
    .then( () => {
      let newUser = new globalUser({
        user: req.body.user,
        password: req.body.password,
        rol: req.body.rol,
        name: req.body.username,
        businessId: req.body.businessId

      });
      newUser.save(function (err) {
        if (err) {
          res.writeHead(400, {'Content-Type': 'text/plain'});
          res.end('Faltan datos.');
        }
      })
      .then( () => {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('Se ha agregado con éxito.');
      });
    });
  }
});


router.put('/api/authenticate-date/:id', (req, res) => {
  credentials.update({ 'businessId': req.params.id}, {$set: {online: req.body.online}}, (err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send(result);
    }
  });
});
module.exports = router;
