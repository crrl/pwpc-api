'use strict';

let express = require('express');
let router = express.Router();

let supplies = require('../models/supplies');
let products = require('../models/products');
let actualSupplies = require('../models/actualSupplies');
let secondarySupplies = require('../models/secondary-supplies');

router.get('/api/supplies', (req, res) => {
  supplies.find((err, result) => {
    if (err) {
      return err;
    } else {
      res.send(result);
    }
  });
});

router.get('/api/suppliesWithDetails', (req, res) => {
  actualSupplies.find((err, result) => {
    if (err) {
      return err;
    } else {
      res.send(result);
    }
  });
});

router.post('/api/supplies', (req, res) => {
  let supplyId;

  let newSupplies = new supplies({
    businessId: req.body.businessId,
    name: req.body.name,
    portionPerProduct: req.body.portionPerProduct,
    type: req.body.type,
    category: req.body.category,
    typeDescription: req.body.typeDescription
  });

  newSupplies.save( (err, result) => {
    if (err) {
      return err;
    } else {
      supplyId = result._id;
    }
  })
  .then(() => {
    let newSupply = new actualSupplies({
      businessId: req.body.businessId,
      supplyId: supplyId,
      name: req.body.name,
      type: req.body.type,
      category: req.body.category,
      portions: 0,
      portionPerProduct: req.body.portionPerProduct,
      typeDescription: req.body.typeDescription,
      quantity: 0
    });

    newSupply.save((err) => {
      if (err) {
        return err;
      }
    })
    .then(() => {
      res.writeHead(200, {'Content-Type': 'text/plain'});
      res.end('Se ha agregado con éxito.');
    })
    .catch(() => {
      return err;
    });
  })
  .catch((err) => {
    return err;
  });
});

router.put('/api/supplies/:id', (req, res) => {
  let quantity = 0;
  actualSupplies.findOne({ 'supplyId': req.params.id }, (err,result) => {
    if (err) {
      return err;
    } else {
      quantity = result.quantity;
    }
  });
  supplies.update({'_id': req.params.id}, req.body, (err, result) => {
    if (err) {
      return err;
    } else {
      req.body.portions = req.body.portionPerProduct * quantity;
      actualSupplies.update({ 'supplyId': req.params.id }, req.body, (err, result) => {
        if (err) {
          return err;
        } else {
          res.writeHead(200, { 'Content-Type': 'text/plain' });
          res.end('Se ha actualizado con éxito.');
        }
      });
    }
  });
});

router.delete('/api/supplies/:id', (req, res) => {
  let remove = false;
  products.find((err, products) => {
    if (err) {
      res.write(err);
    } else {
      products.forEach((product) => {
        product.portions.forEach((portion) => {
          if (portion.id === req.params.id) {
            remove = true;
          }
        });
      });
      if (remove) {
        res.writeHead(500, { 'Content-Type': 'text/plain' });
        res.end('No se puede eliminar el insumo.');
        return;
      } else {
        supplies.remove({'_id': req.params.id}, (err) => {
          if (err) {
            return err;
          }
        })
        .then(() => {
          actualSupplies.remove({'supplyId':req.params.id}, (err) => {
            if (err) {
              return err;
            }
          })
          .then(() => {
            secondarySupplies.remove({'supplyId':req.params.id}, (err) => {
              if (err) {
                return err;
              }
            })
            .then(() => {
              res.writeHead(200, { 'Content-Type': 'text/plain' });
              res.end('Se eliminó correctamente.');
            })
            .catch(() => {
              res.write('Ha ocurrido un error.');
            });
          })
          .catch(() => {
            res.write('Ha ocurrido un error.');
          })
        })
        .catch(() => {
          res.write('Ha ocurrido un error.');
        });
      }
    }
  })
  
});

module.exports = router;
