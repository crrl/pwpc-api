'use strict';

let express = require('express');
let router = express.Router();
let moment = require('moment');
let _ = require("underscore");

let folio = require('../models/folio');
let suppliesDetails = require('../models/supplies-details');

router.post('/api/stadistics', (req, res) => {
  let businessArray = [];
  let clientResponse = {};
  req.body.businessArray.forEach((business) => {
    businessArray.push(business);
  });
  let initialDateProv = '2017-01-01T06:00:00.000Z';
  let finalDateProv = '2050-01-01T06:00:00.000Z';
  let initialDate = new Date((req.body.initialDate || initialDateProv));
  let finalDate = new Date((req.body.finalDate || finalDateProv));
  folio.find({ businessId: { $in: businessArray }, date: {'$gte' : initialDate, '$lt': finalDate }} , (err, result) => {
    if (err) {
      res.write(err);
    } else {
      clientResponse.folios = result;
      
      suppliesDetails.find({ businessId: { $in: businessArray }, date: {'$gte' : initialDate, '$lt': finalDate } }, (err, result) => {
        if (err) {
          res.write('Ha ocurrido un error.');
        } else {
          clientResponse.supplies = result;
          //EMPIEZA
          let fullBusiness = req.body.fullBusiness;
          let productArrayVerifier = 0;
          let sellPerDayVerifier = 0;
          let sellPerMonthVerifier = 0;
          let sellPerHourVerifier = 0;
          let commandsPerHoursVerifier = 0;
          let commandsPerMonthVerifier = 0;
          let diffHours = 0;
          let diffDays = 0;
          let diffMonths = 0;
          let diffFolioHours = 0;
          let diffFolioMonths = 0;
          let initialDate = '';
          let finalDate = '';
          let foliosByBusiness = [];
          let currentLap = 0;
          let productArray = [];
          let nameArray = [];
          let businessName = '';
          let sellPerHour = [];
          let sellPerDay = [];
          let sellPerMonth = [];
          let commandsPerHours = [];
          let commandsPerMonth = [];
          let selectedName = -1;
          let folios = [];
          let itemsToReturn = {
            daySellSeries: [],
            daySellData: [],
            totalMoneyAmmountData: [],
            totalMonthMoneyData: [],
            hourMoneyData: [],
            commandsPerHours: [],
            commandsPerMonth: []
          }
          clientResponse.folios.forEach(function(folio) {
            if (!initialDate) {
              initialDate = folio.date;
            } else if (moment(initialDate).isAfter(folio.date)) {
              initialDate = folio.date;
            }
  
            if (!finalDate) {
              finalDate = folio.date;
            } else if (!moment(finalDate).isAfter(folio.date)) {
              finalDate = folio.date;
            }
          });

          diffHours = Math.ceil((moment(finalDate).diff(moment(initialDate).format('YYYY-MM-DD'),'hours'))/ 24);
          diffDays = Math.ceil((moment(finalDate).diff(moment(initialDate).format('YYYY-MM-DD'),'days') + 2 )/ 7);
          diffMonths = Math.ceil((moment(finalDate).diff(moment(initialDate).format('YYYY-MM-DD'),'months') + 2 )/ 12);
          folios = _.groupBy(clientResponse.folios, function(folio) {
              return folio.businessId +''+ (folio.dayNumber || 1);
          });
          
          for (var key in folios) {
            var nombre = fullBusiness.name;
            folios[key].name = nombre;
            if (!itemsToReturn.daySellSeries.includes(nombre)) itemsToReturn.daySellSeries.push(nombre);
  
            foliosByBusiness.push({
              name: nombre,
              products: []
            });
            folios[key].forEach(function (folio) {
              folio.details.forEach(function (detail) {
                detail.products.forEach(function(product) {
                  var index = _.findIndex(foliosByBusiness[foliosByBusiness.length - 1].products, {productId: product.productId, day: folio.dayNumber});
                  if (index >= 0) {
                    foliosByBusiness[foliosByBusiness.length - 1].products[index].quantity =
                      parseFloat(foliosByBusiness[foliosByBusiness.length - 1].products[index].quantity) +
                      parseFloat(product.quantity);
                  } else {
                    foliosByBusiness[foliosByBusiness.length - 1].products.push({
                      productId: product.productId,
                      productName: product.productName,
                      price: product.price,
                      day: folio.dayNumber || 1,
                      quantity: parseFloat(product.quantity)
                    });
                  }
                });
              });
            });
          }

          var objectKeys = Object.keys(folios);
          for (var index in folios) {
            currentLap += 1;
            if (nameArray.length === 0) {
              nameArray.push(folios[index].name);
              businessName = folios[index].name;
            } else {
              if (businessName !== folios[index].name) {
                businessName = folios[index].name;
                selectedName = nameArray.indexOf(folios[index].name);

                productArray.reduce(function (pastValue, currentValue) {
                  productArrayVerifier += pastValue || 0 + currentValue || 0;
                });

                sellPerDay.reduce(function (pastValue, currentValue) {
                  sellPerDayVerifier += pastValue || 0 + currentValue || 0;
                });

                sellPerMonth.reduce(function (pastValue, currentValue) {
                  sellPerMonthVerifier += pastValue || 0 + currentValue || 0;
                });

                sellPerHour.reduce(function (pastValue, currentValue) {
                  sellPerHourVerifier += pastValue || 0 + currentValue || 0;
                })

                commandsPerHours.reduce(function (pastValue, currentValue) {
                  commandsPerHoursVerifier += pastValue || 0 + currentValue || 0;
                })

                commandsPerMonth.reduce(function (pastValue, currentValue) {
                  commandsPerMonthVerifier += pastValue || 0 + currentValue || 0;
                })

                if ( selectedName < 0) {
                  nameArray.push(folios[index].name);
                  if (productArrayVerifier > 0 || sellPerDayVerifier > 0 || sellPerMonthVerifier > 0
                      || sellPerHourVerifier > 0) {
                    itemsToReturn.daySellData.push(productArray);
                    itemsToReturn.totalMoneyAmmountData.push(sellPerDay);
                    itemsToReturn.totalMonthMoneyData.push(sellPerMonth);
                    itemsToReturn.hourMoneyData.push(sellPerHour);
                    itemsToReturn.commandsPerHours.push(commandsPerHours);
                    itemsToReturn.commandsPerMonth.push(commandsPerMonth);
                    productArray = [];
                    sellPerDay = [];
                    sellPerMonth = [];
                    sellPerHour = [];
                    commandsPerHours = [];
                    commandsPerMonth = [];
                    productArrayVerifier = 0;
                    sellPerDayVerifier = 0;
                    sellPerMonthVerifier = 0;
                    sellPerHourVerifier = 0;
                    commandsPerHoursVerifier = 0;
                    commandsPerMonthVerifier = 0;
                  }
                } else if (productArray.length > 0 &&
                  (productArrayVerifier > 0 || sellPerDayVerifier > 0 || sellPerMonthVerifier > 0
                  || sellPerHourVerifier > 0)) {
                  itemsToReturn.daySellData.push(productArray);
                  itemsToReturn.totalMoneyAmmountData.push(sellPerDay);
                  itemsToReturn.totalMonthMoneyData.push(sellPerMonth);
                  itemsToReturn.hourMoneyData.push(sellPerHour);
                  itemsToReturn.commandsPerHours.push(commandsPerHours);
                  itemsToReturn.commandsPerMonth.push(commandsPerMonth);
                  productArray = [];
                  sellPerDay = [];
                  sellPerMonth = [];
                  sellPerHour = [];
                  commandsPerHours = [];
                  commandsPerMonth = [];
                  productArrayVerifier = 0;
                  sellPerDayVerifier = 0;
                  sellPerMonthVerifier = 0;
                  sellPerHourVerifier = 0;
                  commandsPerHoursVerifier = 0;
                  commandsPerMonthVerifier = 0;
                }
              }
            }
            while (productArray.length < 7) {
              productArray.push(0);
              sellPerDay.push(0);
            }
            while(sellPerMonth.length < 12) {
              sellPerMonth.push(0);
              commandsPerMonth.push(0);
            }

            while(sellPerHour.length < 24) {
              sellPerHour.push(0);
              commandsPerHours.push(0);
            }

            if (folios[index][0].dayNumber === 0) {
              folios[index][0].dayNumber = 1;
            }
            if (selectedName >= 0) {
              itemsToReturn.daySellData[selectedName][folios[index][0].dayNumber - 1 ] = folios[index].length / diffDays;
              folios[index].forEach( function(folio) {
                itemsToReturn.commandsPerHours[selectedName][(parseInt(moment(folio.date).format('H')))] += 1 / diffHours;
                itemsToReturn.commandsPerMonth[selectedName][(parseInt(moment(folio.date).format('M')))-1] += 1 / diffMonths;
                itemsToReturn.totalMonthMoneyData[selectedName][parseInt(moment(folio.date).format('M'))-1 ] += folio.total / diffMonths;
                itemsToReturn.hourMoneyData[selectedName][(parseInt(moment(folio.date).format('H')))] += folio.total / diffHours;
                itemsToReturn.totalMoneyAmmountData[selectedName][folios[index][0].dayNumber - 1 ] += folio.total / diffDays;
              });
            } else {
              productArray[folios[index][0].dayNumber - 1 ] = folios[index].length / diffDays;
              folios[index].forEach( function(folio) {
                commandsPerHours[parseInt(moment(folio.date).format('H'))] += 1 / diffHours;
                commandsPerMonth[parseInt(moment(folio.date).format('M')) - 1] += 1 / diffMonths;
                sellPerHour[parseInt(moment(folio.date).format('H')) ] += folio.total / diffHours;
                sellPerMonth[parseInt(moment(folio.date).format('M'))- 1] += folio.total / diffMonths;
                sellPerDay[folios[index][0].dayNumber - 1 ] += folio.total / diffDays ;
              });
            }

            if (currentLap === objectKeys.length) {
              productArrayVerifier = 0;
              productArray.reduce(function (pastValue, currentValue) {
                productArrayVerifier += pastValue || 0 + currentValue || 0;
              });
              sellPerDayVerifier = 0;
              sellPerDay.reduce(function (pastValue, currentValue) {
                sellPerDayVerifier += pastValue || 0 + currentValue || 0;
              });
              sellPerMonthVerifier = 0;
              sellPerMonth.reduce(function (pastValue, currentValue) {
                sellPerMonthVerifier += pastValue || 0 + currentValue || 0;
              });
              sellPerHourVerifier = 0;
              sellPerHour.reduce(function (pastValue, currentValue) {
                sellPerHourVerifier += pastValue || 0 + currentValue || 0;
              });

              commandsPerHoursVerifier = 0;
              commandsPerHours.reduce(function (pastValue, currentValue) {
                commandsPerHoursVerifier += pastValue || 0 + currentValue || 0;
              });

              commandsPerMonthVerifier = 0;
              commandsPerMonth.reduce(function (pastValue, currentValue) {
                commandsPerMonthVerifier += pastValue || 0 + currentValue || 0;
              });

              if (productArrayVerifier > 0 || sellPerDayVerifier > 0 || sellPerMonthVerifier > 0
                  || sellPerHourVerifier > 0) {
                itemsToReturn.daySellData.push(productArray);
                itemsToReturn.totalMoneyAmmountData.push(sellPerDay);
                itemsToReturn.totalMonthMoneyData.push(sellPerMonth);
                itemsToReturn.hourMoneyData.push(sellPerHour);
                itemsToReturn.commandsPerHours.push(commandsPerHours);
                itemsToReturn.commandsPerMonth.push(commandsPerMonth);
              }
            }
          }  

          itemsToReturn.daySellData.forEach((items, itemsIndex) => {
              items.forEach((item,index) => {
                itemsToReturn.daySellData[itemsIndex][index] = Math.ceil(item*100)/100;
              });
          });

          itemsToReturn.totalMoneyAmmountData.forEach((items, itemsIndex) => {
            items.forEach((item,index) => {
              itemsToReturn.totalMoneyAmmountData[itemsIndex][index] = Math.ceil(item*100)/100;
            });
          });
          
          itemsToReturn.totalMonthMoneyData.forEach((items, itemsIndex) => {
            items.forEach((item,index) => {
              itemsToReturn.totalMonthMoneyData[itemsIndex][index] = Math.ceil(item*100)/100;
            });
          });

          itemsToReturn.hourMoneyData.forEach((items, itemsIndex) => {
            items.forEach((item,index) => {
              itemsToReturn.hourMoneyData[itemsIndex][index] = Math.ceil(item*100)/100;
            });
          });

          itemsToReturn.commandsPerHours.forEach((items, itemsIndex) => {
            items.forEach((item,index) => {
              itemsToReturn.commandsPerHours[itemsIndex][index] = Math.ceil(item*100)/100;
            });
          });

          itemsToReturn.commandsPerMonth.forEach((items, itemsIndex) => {
            items.forEach((item,index) => {
              itemsToReturn.commandsPerMonth[itemsIndex][index] = Math.ceil(item*100)/100;
            });
          });

          //clientResponse.graphics = itemsToReturn;
          res.send(itemsToReturn);
        }
      });
    }
  });
});

module.exports = router;
