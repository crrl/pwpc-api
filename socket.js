var socket_io = require('socket.io');
var os = require( 'os' );

var networkInterfaces = os.networkInterfaces();
var io = socket_io();
var socketApi = {};

socketApi.io = io;

function sendHeartbeat(){
  io.sockets.emit('ping', { beat : 1 });
}

io.on('connection', function (socket) {
  setTimeout(sendHeartbeat, 3000);
  console.log('connected');
});
io.sockets.on('pong', function(data){
    console.log("Pong received from client");
});

  setTimeout(sendHeartbeat, 3000);


module.exports = socketApi;
