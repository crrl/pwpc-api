'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let payDesk = new Schema({
  fundAvailable : Number,
  previousFund: Number,
  withdrawal: Number,
  deposit: Number,
  sells: Number,
  compras: Number,
  rosters: Number,
  cashFund: Number,
  date: { type:Date, default: Date.now },
  online: { type: Boolean, default: false }
});

module.exports = mongoose.model('payDesk', payDesk);
