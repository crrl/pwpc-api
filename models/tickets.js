'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let dayNumber = new Date().getDay();

let tickets = new Schema({
  businessId: String,
  userId: String,
  userName: String,
  total: Number,
  table: Number,
  folio: { type: String, default: 0 },
  folioNumber: { type: Number, default: 0 },
  products: [{}],
  initialHour: String,
  endHour: String,
  totalTime: Number,
  date: { type: Date, default: Date.now },
  dayNumber: {type: Number, default: dayNumber},
  online: { type: Boolean, default: false }
});

module.exports = mongoose.model('tickets', tickets);
