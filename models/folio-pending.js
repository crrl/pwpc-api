'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let folioPending = new Schema({
  businessId: String,
  folioId: String,
  table: Number,
  userId: String,
  userName: String,
  total: Number,
  table: Number,
  folio: { type: Number, default: 0 },
  pendingPayment: Number,
  details: [{}],
  ticket: {type: Boolean, default: false},
  date: { type: Date, default: Date.now }
});

module.exports = mongoose.model('folioPending', folioPending);
