'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

let dayNumber = new Date().getDay();

var rosterDetails = new Schema({
  businessId: String,
  userId: String,
  username: String,
  workedDays: Number,
  extraTime: Number,
  payPerExtraHour: Number,
  absenceDays: Number,
  total: Number,
  payPerDay: Number,
  dayNumber: {type: Number, default: dayNumber},
  initialDate: { type: Date, default: Date.now },
  online: { type: Boolean, default: false }
});

module.exports = mongoose.model('rosterDetails', rosterDetails);
