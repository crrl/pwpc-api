'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let servingPendings = new Schema({
  folioId: String,
  table: Number,
  served: String,
  details: [{}]
});

module.exports = mongoose.model('servingPendings', servingPendings);
