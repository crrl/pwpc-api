'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var randomNumber = Math.floor(Math.random() * 1000000);

var business = new Schema({
  name : String,
  mail : String,
  address: String,
  phone: String,
  autogen: { type: Number, default: randomNumber },
  online: { type: Boolean, default: false }
});

module.exports = mongoose.model('Business', business);
