'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let productsCategory = new Schema({
  category: String,
  image: String,
  printer: String
});

module.exports = mongoose.model('productsCategory', productsCategory);
