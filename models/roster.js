'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var roster = new Schema({
  userId : String,
  businessId: String,
  payPerDay: Number,
  online: { type: Boolean, default: false }
});

module.exports = mongoose.model('roster', roster);
