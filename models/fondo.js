'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let fondo = new Schema({
  businessId: String,
  userId: String,
  quantity: Number,
  cutId: String,
  date: { type: Date, default: Date.now },
  salesCut: { type: Boolean, default: false },
  turnCut: { type: Boolean, default: false },
  online: { type: Boolean, default: false }
});

module.exports = mongoose.model('fondo', fondo);
