'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let secundarySupplies = new Schema({
  businessId: String,
  supplyId: String,
  name: String,
  supplyName: String,
  portions: { type: Number, default: 0 },
  date: { type: Date, default: Date.now }
});

module.exports = mongoose.model('secundarySupplies', secundarySupplies);
