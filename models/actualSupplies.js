'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let actualSupplies = new Schema({
  businessId: String,
  supplyId: String,
  name: String,
  type: String,
  category: String,
  portions: { type: Number, default: 0},
  portionPerProduct: { type: Number, default: 0},
  typeDescription: String,
  quantity: Number
});

module.exports = mongoose.model('actualSupplies', actualSupplies);
