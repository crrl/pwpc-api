'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

let dayNumber = new Date().getDay();

var boughtDetails = new Schema({
    userId: String,
    boughtId: String,
    businessId: String,
    productId: String,
    quantity: Number,
    total: Number,
    dayNumber: {type: Number, default: dayNumber},
    date: { type: Date, default: Date.now },
    online: { type: Boolean, default: false }
});

module.exports = mongoose.model('boughtDetails', boughtDetails);
