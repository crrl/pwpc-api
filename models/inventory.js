'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

let dayNumber = new Date().getDay();


var inventory = new Schema({
  businessId:String,
  userId: String,
  userName: String,
  details: [{}],
  dayNumber: {type: Number, default: dayNumber},
  date:{ type: Date, default: Date.now },
  online: { type: Boolean, default: false }
});

module.exports = mongoose.model('inventory', inventory);
