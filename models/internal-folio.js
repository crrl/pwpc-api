'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let dayNumber = new Date().getDay();

let internalFolio = new Schema({
  businessId: String,
  userId: String,
  userName: String,
  total: Number,
  table: Number,
  folio: { type: Number, default: 0 },
  status: String,
  userCanceledId: String,
  cancelUser: String,
  details: [{}],
  internalFood: { type: Boolean, default: false },
  internalUser: { type: String, default: '' },
  totalTickets: { type: Number, default: 0 },
  date: { type: Date, default: Date.now },
  dayNumber: {type: Number, default: dayNumber},
  salesCut: { type: Boolean, default: false },
  cutId: { type: String, default: '' },
  online: { type: Boolean, default: false }
});

module.exports = mongoose.model('internalFolio', internalFolio);
