'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let config = new Schema({
  businessId: String,
  userId : String,
  color: String,
  background: String,
  sellUI: Number,
  printOrders: Boolean,
  saveOrders: Boolean
});

module.exports = mongoose.model('config', config);
