'use strict';
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let rosterHistory = new Schema({
  businessId: String,
  rosterId: String,
  userId: String,
  username: String,
  workedDays: Number,
  absenceDays: Number,
  extraTime: Number,
  total: Number,
  initialDate: Date,
  endDate: { type: Date, default: Date.now },
  salesCut: { type: Boolean, default: false },
  turnCut: { type: Boolean, default: false },
  cutId: { type: String, default: '' },
  online: { type: Boolean, default: false }
});

module.exports = mongoose.model('rosterHistory', rosterHistory);
