'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let actualPrinters = new Schema({
  name: String
});

module.exports = mongoose.model('actualPrinters', actualPrinters);
