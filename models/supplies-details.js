'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let dayNumber = new Date().getDay();

let suppliesDetails = new Schema({
  businessId: String,
  userId: String,
  products: [{}],
  total: Number,
  date: { type: Date, default: Date.now },
  folio: { type: Number, default: 0 },
  dayNumber: {type: Number, default: dayNumber},
  salesCut: { type: Boolean, default: false },
  turnCut: { type: Boolean, default: false },
  cutId: { type: String, default: '' }
});

module.exports = mongoose.model('suppliesDetails', suppliesDetails);
