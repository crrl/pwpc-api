'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
let dayNumber = new Date().getDay();

var salesCut = new Schema({
  businessId: String,
  userId: String,
  userName: String,
  products: [{}],
  boughts: [{}],
  rosters: [{}],
  folios: [],
  supplies: [],
  fondo: [{}],
  internFolios: [],
  totalRoster: Number,
  entries: Number,
  Outputs: Number,
  type: String,
  anotations: String,
  dayNumber: {type: Number, default: dayNumber},
  date: { type: Date, default: Date.now },
  cutId: String,
  online: { type: Boolean, default: false }
});

module.exports = mongoose.model('salesCut', salesCut);
