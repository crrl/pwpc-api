'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var globalUsers = new Schema({
  businessId: String,
  user : String,
  name: String,
  password : String,
  rol: String,
  joinDate: { type: Date, default: Date.now },
  online: { type: Boolean, default: false }
});

module.exports = mongoose.model('globalusers', globalUsers);
