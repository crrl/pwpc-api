'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var modules = new Schema({
  moduleName: String,
  moduleAPI: String
});

module.exports = mongoose.model('modules', modules);
