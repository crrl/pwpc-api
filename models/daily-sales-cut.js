'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
let dayNumber = new Date().getDay();

var dailySalesCut = new Schema({
  businessId: String,
  userId: String,
  userName: String,
  products: [{}],
  boughts: [{}],
  folios: [],
  rosters: [{}],
  supplies: [],
  entries: Number,
  Outputs: Number,
  fondo: [{}],
  type: String,
  totalRoster: Number,
  anotations: String,
  dayNumber: {type: Number, default: dayNumber},
  date: { type: Date, default: Date.now },
  online: { type: Boolean, default: false }
});

module.exports = mongoose.model('dailySalesCut', dailySalesCut);
