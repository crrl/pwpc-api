'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let supplies = new Schema({
  businessId: String,
  name: String,
  type: String,
  typeDescription: String,
  category: String,
  quantity: { type: Number, default: 0 },
  portions: { type: Number, default: 0 },
  portionPerProduct: { type: Number, default: 0 },
  date: { type: Date, default: Date.now }
});

module.exports = mongoose.model('supplies', supplies);
