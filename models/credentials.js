'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let credentials = new Schema({
  businessId : String,
  token : String,
  online: { type: Boolean, default: false}
});

module.exports = mongoose.model('credentials', credentials);
