'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var products = new Schema({
  businessId: String,
  name: String,
  price: Number,
  portions: [{}],
  categoryId: String,
  printer: String
});

module.exports = mongoose.model('products', products);
