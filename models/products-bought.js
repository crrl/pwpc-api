'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var boughts = new Schema({
  userId: String,
  businessId: String,
  total: Number,
  online: { type: Boolean, default: false }
});

module.exports = mongoose.model('boughts', boughts);
