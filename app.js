'use strict';

let express = require('express');
let path = require('path');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
let jwt    = require('jsonwebtoken');
let cors = require('cors')
let socket = require('socket.io');
mongoose.connect('mongodb://wendy:welcome@127.0.0.1:27017/pwpc', {
  auto_reconnect: true,
  db: {
    w: 1
  },
  server : {
    reconnectTries : Number.MAX_VALUE,
    autoReconnect : true,
    socketOptions: {
      keepAlive: 1
    }
  }
}, function (err, db) {

});
let app = module.exports = express();

//routes
let authenticate = require('./routes/user-authenticate');
let globalUser = require('./routes/users');
let business = require('./routes/business');
let roster = require('./routes/roster-details');
let rosterHistory = require('./routes/roster-history');
let supplies = require('./routes/supplies');
let products = require('./routes/products');
let suppliesDetails = require('./routes/supplies-details');
let inventory = require('./routes/inventory');
let folio = require('./routes/folio');
let salesCut = require('./routes/daily-total-ammount');
let fondo = require('./routes/fondo');
let paydesk = require('./routes/paydesk');
let tickets = require('./routes/tickets');
let UIConfig = require('./routes/UI-config');
let productsCategory = require('./routes/products-category');
let printers = require('./routes/printers');
let secondarySupplies = require('./routes/secondary-supplies');
let stadistics = require('./routes/stadistics');
let telebot = require('./routes/telegram-bot');
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.set('secretKey', 'superduper');
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
  if (req.url === '/api/authenticate' ||
      req.url.indexOf('/api/business') >= 0 || 
      req.url.indexOf('/api/authenticate-date') >= 0) {
    next();
  } else {
    jwt.verify(req.headers.token, app.get('secretKey'), function (err) {
      if (err) {
        res.status(500).send({'access': false, 'status':401});

      } else {
        next();
      }
    });
  }
});

app.use(function (req, res, next) {
  console.log(mongoose.connection.readyState);
  if (mongoose.connection.readyState !== 1 && mongoose.connection.readyState !== 2) {
    mongoose.connect('mongodb://wendy:welcome@127.0.0.1:27017/pwpc', {
      auto_reconnect: true,
      db: {
        w: 1
      },
      server : {
        reconnectTries : Number.MAX_VALUE,
        autoReconnect : true,
        socketOptions: {
          keepAlive: 1
        }
      }
    }, function (err, db) {

    });
  }
  next();
});

app.use(authenticate);
app.use(globalUser);
app.use(business);
app.use(roster);
app.use(rosterHistory);
app.use(supplies);
app.use(products)
app.use(suppliesDetails);
app.use(inventory);
app.use(folio);
app.use(salesCut);
app.use(fondo);
app.use(paydesk);
app.use(UIConfig);
app.use(tickets);
app.use(productsCategory);
app.use(printers);
app.use(secondarySupplies);
app.use(stadistics);
app.use(telebot);
